import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { MediaService } from './media.service';
import { MediaController } from './media.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
	UploadFileSchema,
	UploadFileModel,
} from './schemas/upload-file.schema';
import { LogModule } from "@/log/log.module";

@Module({
	imports: [
		CommonModule,
		MongooseModule.forFeature([
			{ name: UploadFileModel.name, schema: UploadFileSchema },
		]),
		LogModule
	],
	providers: [MediaService],
	exports: [MediaService],
	controllers: [MediaController],
})
export class MediaModule { }
