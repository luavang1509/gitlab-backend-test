import { Injectable } from '@nestjs/common';
import * as csv from 'csv-parser';
import { Readable } from 'stream';
import * as fs from 'fs-extra';
import { ConfigService } from '@nestjs/config';
import * as AWS from 'aws-sdk';
import {
	generateFileName,
	getMimeFromFileName,
	getExtFromFileName,
} from '@/utils';
import { InjectModel } from '@nestjs/mongoose';
import {
	UploadFileDocument,
	UploadFileModel,
} from './schemas/upload-file.schema';
import { Model } from 'mongoose';
import { LogService } from "@/log/log.service";
import { LogType } from "@/constants";

@Injectable()
export class MediaService {
	private s3: AWS.S3;
	constructor(
		private readonly configService: ConfigService,
		@InjectModel(UploadFileModel.name)
		private uploadFileModel: Model<UploadFileDocument>,
		private readonly logService: LogService
	) { }

	parseFileData(file: Express.Multer.File, fieldsName: string[]) {
		return new Promise((resolve, reject) => {
			const results = [];
			const options: any = {
				skipLines: 1,
				separator: ',',
				headers: fieldsName,
			};

			Readable.from(fs.createReadStream(file.path))
				.pipe(csv(options))
				.on('data', (data) => {
					const item = {};
					fieldsName.forEach((v) => {
						if (data[v]) {
							item[v] = data[v];
						}
					});
					results.push(item);
				})
				.on('error', (err) => reject(err))
				.on('end', () => resolve(results));
		});
	}

	getS3() {
		if (!this.s3) {
			this.s3 = new AWS.S3({
				accessKeyId: this.configService.get('AWS_ACCESS_KEY_ID'),
				secretAccessKey: this.configService.get('AWS_SECRET_ACCESS_KEY'),
				region: this.configService.get('AWS_REGION'),
			});
		}
		return this.s3;
	}

	async sendFileToS3(fileStream: any, file: Express.Multer.File) {
		const { originalname } = file;
		const ext = getExtFromFileName(originalname);
		const name = generateFileName(ext);
		const mime = getMimeFromFileName(originalname);
		const bucket = this.configService.get('AWS_BUCKET') + (mime.includes('image') ? '/images' : '/csv');
		const params = {
			Bucket: bucket,
			Key: name,
			Body: fileStream,
			ContentType: mime,
			Metadata: { mimetype: mime },
			CacheControl: 'max-age=3600',
		};
		const uploaded = await this.getS3().upload(params).promise();

		if (uploaded) {
			this.logService.create({
				message: `Uploaded file ${originalname} to S3`,
				type: LogType.INFO,
			});
			const [hash] = name.split('.');
			const { size } = file;
			const url = await this.getS3().getSignedUrlPromise('getObject', {
				Bucket: bucket,
				Key: name,
				Expires: 60 * 60 * 24 * 7,
			});
			const ext = getExtFromFileName(name);
			await this.uploadFileModel.create({
				name,
				mime,
				hash,
				url,
				ext,
				size,
				isRemoved: 0,
			});

			return { name, url, ext, hash, mime, size };
		}
		this.logService.create({
			message: 'Error while uploading file to S3',
			type: LogType.ERROR,
		});
		return null;
	}

	async uploadCsvContentToS3(csvContent: string) {
		const bucket = this.configService.get('AWS_BUCKET') + '/csv';
		const name = generateFileName('.csv');
		const mime = 'text/csv';
		const params = {
			Bucket: bucket,
			Key: name,
			Body: csvContent,
			ContentType: mime,
			Metadata: { mimetype: 'text/csv' },
			CacheControl: 'max-age=3600',
		};
		const uploaded = await this.getS3().upload(params).promise();

		if (uploaded) {
			this.logService.create({
				message: `Uploaded csv content to S3`,
				type: LogType.INFO,
			});
			const [hash] = name.split('.');
			const url = await this.getS3().getSignedUrlPromise('getObject', {
				Bucket: bucket,
				Key: name,
				Expires: 60 * 60 * 24 * 7,
			});
			const ext = getExtFromFileName(name);
			await this.uploadFileModel.create({
				name,
				mime,
				hash,
				url,
				ext,
				size: 0,
				isRemoved: 0,
			});

			return { name, url, ext, hash, mime, size: 0 };
		}
		this.logService.create({
			message: 'Error while uploading csv to S3',
			type: LogType.ERROR,
		});

		return null;
	}

	async upload(file: Express.Multer.File) {
		const { path } = file;
		const fileStream = fs.createReadStream(path);
		return this.sendFileToS3(fileStream, file);
	}
}
