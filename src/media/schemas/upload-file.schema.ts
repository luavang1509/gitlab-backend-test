import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UploadFileDocument = UploadFileModel & Document;

@Schema({ timestamps: true, collection: 'upload_files' })
export class UploadFileModel {
	@Prop({ type: String, required: true })
	name: string;
	@Prop({ ttype: String, required: true, index: true, unique: true })
	hash: string;
	@Prop({ type: String, required: true })
	mime: string;
	@Prop({ type: String, required: true })
	ext: string;
	@Prop({ type: String, required: true, index: true, unique: true })
	url: string;
	@Prop({ type: Number, default: 0 })
	size: number;
	@Prop({ type: Number, default: 0 })
	isRemoved: number;
	@Prop()
	createdAt?: Date;
	@Prop()
	updatedAt?: Date;
}

export const UploadFileSchema = SchemaFactory.createForClass(UploadFileModel);
