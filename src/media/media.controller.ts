import {
	Controller,
	Logger,
	Post,
	UseInterceptors,
	UploadedFile,
	Req,
	BadRequestException,
} from '@nestjs/common';
import { Express } from 'express';
import { MediaService } from './media.service';
import { UploadCSV, UploadImage } from './upload.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('media')
export class MediaController {
	private readonly logger = new Logger(MediaController.name);
	constructor(private readonly mediaService: MediaService) { }

	@Post('/csv')
	@UseInterceptors(FileInterceptor('file', UploadCSV))
	async uploadCsv(
		@Req() req,
		@UploadedFile() file: Express.Multer.File,
	): Promise<{
		data: {
			name: string;
			url: string;
			ext: string;
			hash: string;
			mime: string;
			size: number;
		};
	}> {
		if (req.fileValidationError) {
			throw new BadRequestException(req.fileValidationError);
		}
		const data = await this.mediaService.upload(file);
		return { data };
	}

	@Post('/image')
	@UseInterceptors(FileInterceptor('file', UploadImage))
	async uploadImage(
		@Req() req,
		@UploadedFile() file: Express.Multer.File,
	): Promise<{
		data: {
			name: string;
			url: string;
			ext: string;
			hash: string;
			mime: string;
			size: number;
		};
	}> {
		if (req.fileValidationError) {
			throw new BadRequestException(req.fileValidationError);
		}
		const data = await this.mediaService.upload(file);
		return { data };
	}
}
