import { diskStorage } from 'multer';
import { md5, generateUUIDWithMoment, getFileExt } from '@/utils';
import * as path from 'path';
import * as fs from 'fs-extra';
import * as moment from 'moment-timezone';
import { MessageError } from '../enums';

const MAX_SIZE = 1024 * 1024 * 1; // 1mb
const USER_CACHE_IMAGE_DIRECTORY = path.resolve(
	__dirname,
	'../../',
	path.join('data', '.cache', '.images'),
);
const USER_CACHE_FILE_DIRECTORY = path.resolve(
	__dirname,
	'../../',
	path.join('data', '.cache', '.files'),
);

let __currentImageCacheDirPath = null;
let __currentFileCacheDirPath = null;

async function getImageCacheDirectory() {
	const directory = path.join(
		USER_CACHE_IMAGE_DIRECTORY,
		moment().format('YYYY-MM-DD'),
	);
	if (
		__currentImageCacheDirPath === null ||
		__currentImageCacheDirPath !== directory
	) {
		await fs.ensureDir(directory);
		__currentImageCacheDirPath = directory;
		return directory;
	}

	return __currentImageCacheDirPath;
}

async function getFileCacheDirectory() {
	const directory = path.join(
		USER_CACHE_FILE_DIRECTORY,
		moment().format('YYYY-MM-DD'),
	);
	if (
		__currentFileCacheDirPath === null ||
		__currentFileCacheDirPath !== directory
	) {
		await fs.ensureDir(directory);
		__currentFileCacheDirPath = directory;
		return directory;
	}

	return __currentFileCacheDirPath;
}

const FileDiskStorage = diskStorage({
	destination: async function (_request, file: any, cb) {
		try {
			const filePath = await getFileCacheDirectory();
			file.filePath = filePath;
			cb(null, filePath);
		} catch (e) {
			cb(e, null);
		}
	},
	filename: (request, file, cb) => getFileName(request, file, cb),
});

const ImageDiskStorage = diskStorage({
	destination: async function (_request, file: any, cb) {
		try {
			const filePath = await getImageCacheDirectory();
			file.filePath = filePath;
			cb(null, filePath);
		} catch (e) {
			cb(e, null);
		}
	},
	filename: (request, file, cb) => getFileName(request, file, cb),
});

function getFileName(_request, file: any, cb) {
	const extend = getFileExt(file.originalname);
	const hash = md5(generateUUIDWithMoment());
	file.hash = hash;
	file.name = hash + extend.normalize;
	file.ext = extend.normalize;
	file.filePath = path.join(file.filePath, file.name);
	cb(null, file.name);
}

export const UploadImage = {
	limits: {
		fieldNameSize: 1024,
		files: 1,
		fileSize: MAX_SIZE,
	},
	storage: ImageDiskStorage,
	fileFilter: async (req, file, cb) => {
		const mimetype = file.mimetype.toLowerCase();
		if (
			!mimetype.includes('jpeg') &&
			!mimetype.includes('jpg') &&
			!mimetype.includes('png')
		) {
			req.fileValidationError = MessageError['IMAGE_INVALID_FORMAT'];
			return cb(null, false, MessageError['IMAGE_INVALID_FORMAT']);
		} else {
			cb(null, true);
		}
	},
};

export const UploadCSV = {
	limits: {
		fieldNameSize: 1024,
		files: 1,
		fileSize: MAX_SIZE,
	},
	fileFilter: async (req, file, cb) => {
		const mimetype = file.mimetype.toLowerCase();
		if (mimetype.includes('csv') || mimetype.includes('vnd.ms-excel')) {
			cb(null, true);
		} else {
			req.fileValidationError = MessageError['CSV_INVALID_FORMAT'];
			return cb(null, false, MessageError['CSV_INVALID_FORMAT']);
		}
	},
	storage: FileDiskStorage,
};
