import { CommonModule } from "@/common/common.module";
import { DAOModule } from "@/dao/dao.module";
import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
	imports: [CommonModule, DAOModule],
	providers: [AuthService],
	controllers: [AuthController]
})
export class AuthModule { }
