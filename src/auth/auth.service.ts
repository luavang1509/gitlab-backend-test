import {
	BadRequestException,
	Injectable,
	Logger,
	NotFoundException,
} from '@nestjs/common';
import { UserService } from "@/dao/services/user.service";
import { generateToken, validatePassword } from "@/utils";
import { HttpRequestUtils } from "@taptap-discovery/http-request-utils";
import { ConfigTokenRequestParam, RequestParams } from "@taptap-discovery/http-request-utils/lib/type";
import { ADMIN_ACCESS_TOKEN_PREFIX, USER_ACCESS_TOKEN_PREFIX } from '@/constants';
import { RedisService } from "@/common/services/redis.service";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class AuthService {
	private logger = new Logger(AuthService.name);
	private readonly request = new HttpRequestUtils();

	constructor(
		private userService: UserService,
		private redisService: RedisService,
		private readonly configService: ConfigService,
	) { }

	async login(dto) {
		const user = await this.userService.findByUsername(dto.username);

		if (!user) {
			throw new BadRequestException('Invalid username');
		}

		if (!validatePassword(dto.password, user.password)) {
			throw new BadRequestException('Invalid password');
		}

		return {
			_id: user._id,
			_username: user.username
		};
	}

	async generateAccessToken(user: any) {
		const token = generateToken();
		const key = `${ADMIN_ACCESS_TOKEN_PREFIX}.${token}`;
		const data = {
			_id: user._id,
			email: user.email,
			displayName: user.displayName,
			group: user.group,
		};
		const ttl = 24 * 3600 * 1000;

		await this.redisService.setEx(key, JSON.stringify(data), ttl);

		return token;
	}

	async generateUserAccessToken(user: any) {
		const token = generateToken();
		const key = `${USER_ACCESS_TOKEN_PREFIX}.${token}`;
		const data = {
			userId: user.userId,
			mobile: user.mobile,
		};
		const ttl = 24 * 3600 * 1000;

		await this.redisService.setEx(key, JSON.stringify(data), ttl);
		return token;
	}

	async removeAccessToken(token, tokenPrefix = ADMIN_ACCESS_TOKEN_PREFIX) {
		const key = `${tokenPrefix}.${token}`;
		await this.redisService.del(key);
	}

	async loginToken(token: string) {
		const options: RequestParams = {
			url: `http://localhost:3002/verify_token?token=${token}`,
			method: 'GET'
		};
		const request = await this.request.request(options);
		return request;
	}
}