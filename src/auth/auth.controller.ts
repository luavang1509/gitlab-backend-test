import {
	Body,
	Controller,
	Post,
} from '@nestjs/common';
import { AuthService } from './auth.service';

@Controller('/auth')
export class AuthController {
	constructor(private authService: AuthService) { }

	@Post('/sign-in')
	async login(@Body() body) {
		const user = await this.authService.login(body);
		const accessToken = await this.authService.generateAccessToken(user);
		return {
			data: { ...user, accessToken },
		};
	}

	@Post('/sign-in-token')
	async loginToken(@Body() body) {
		const request = await this.authService.loginToken(body.token);
		if (request?.status?.code === 200) {
			const user = request.data;
			const token = await this.authService.generateUserAccessToken(user);
			return {
				data: {
					token,
				},
			};
		}
		return request;
	}
}
