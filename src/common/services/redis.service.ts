import type { ConfigSchema } from '@/config/env.validation';
import {
	Injectable,
	Logger,
	OnApplicationBootstrap,
	OnApplicationShutdown,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as redis from 'redis';
//this is similar to const redis = require('redis');
@Injectable()
export class RedisService
	implements OnApplicationShutdown, OnApplicationBootstrap {
	private logger = new Logger(RedisService.name);
	private client;

	constructor(private configService: ConfigService<ConfigSchema>) { }

	async onApplicationBootstrap() {
		await this.connect();
	}

	async onApplicationShutdown() {
		await this.disconnect();
	}

	async connect() {


		// const client = redis.createClient(options);
		let client = redis.createClient({
			socket: {
				host: this.configService.get<string>('REDIS_HOST'),
				port: this.configService.get<number>('REDIS_PORT'),
			},
		});

		//TODO: find out why this is working
		(async () => {
			await client.connect();
		})();

		client.on('connect', () => {
			this.logger.log('Redis is connecting');
		});

		client.on('ready', () => {
			this.logger.log('Redis is ready');
		});

		client.on('error', (error) => {
			this.logger.error(error.message);
		});


		this.client = client;
	}

	async disconnect() {
		if (this.client && this.client.connected) {
			await this.client.quit();
		}
	}

	// Redis commands

	get(key: string) {
		return this.client.get(key);
	}

	set(key: string, value: unknown) {
		return this.client.set(key, value);
	}

	setEx(key: string, value: unknown, expire: number) {
		return this.client.set(key, value, {
			EX: expire,
		});
	}

	setExNx(key: string, value: unknown, expire: number) {
		return this.client.set(key, value, {
			EX: expire,
			NX: 10,
		});
	}

	del(key: string) {
		return this.client.del(key);
	}
}
