import { DAOModule } from '@/dao/dao.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RedisService } from './services/redis.service';

@Module({
	imports: [ConfigModule, DAOModule],
	exports: [RedisService],
	providers: [
		RedisService,
	],
})
export class CommonModule { }
