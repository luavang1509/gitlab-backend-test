import { Module } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MerchandiseModule } from './merchandise/merchandise.module';
import { AddressModule } from "./address/address.module";
import { OrderModule } from "./order/order.module";
import { AuthModule } from "./auth/auth.module";
import { MediaModule } from "./media/media.module";
import { ExportHistoryModule } from "./export-history/export-history.module";

@Module({
	imports: [
		ConfigModule,
		MerchandiseModule,
		AddressModule,
		AuthModule,
		OrderModule,
		MediaModule,
		ExportHistoryModule,
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule { };
