import { UserAuthGuard } from "@/guards/user-auth.guard";
import { Controller, Get, Param, UseGuards, Request } from '@nestjs/common';
import { MerchandiseService } from './merchandise.service';

@Controller('merchandise')
export class MerchandiseController {
	constructor(private readonly merchandiseService: MerchandiseService) { }
	@Get('my-coupon')
	@UseGuards(UserAuthGuard)
	getMerchandiseByUserId(@Request() req) {
		return this.merchandiseService.getAllCoupons(req.user.mobile)
	}
}
