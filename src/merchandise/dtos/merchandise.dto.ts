import { IsNotEmpty, IsString } from 'class-validator';

export class MerchandiseDto {
	@IsNotEmpty()
	@IsString()
	voucherCode: string;

	@IsNotEmpty()
	@IsString()
	rewardName: string;

	@IsNotEmpty()
	@IsString()
	brandCode: string;

	@IsNotEmpty()
	@IsString()
	offerId: string;
}
