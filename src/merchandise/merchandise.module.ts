import { AuthModule } from "@/auth/auth.module";
import { CommonModule } from "@/common/common.module";
import { LogModule } from "@/log/log.module";
import { Module } from '@nestjs/common';
import { MerchandiseController } from './merchandise.controller';
import { MerchandiseService } from './merchandise.service';

@Module({
	imports: [AuthModule, CommonModule, LogModule],
	controllers: [MerchandiseController],
	providers: [MerchandiseService],
	exports: [MerchandiseService],
})
export class MerchandiseModule { }