import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from "@nestjs/config";
import { HttpRequestUtils } from "@taptap-discovery/http-request-utils";
import { ConfigTokenRequestParam, RequestParams } from "@taptap-discovery/http-request-utils/lib/type";
import { MerchandiseDto } from "./dtos/merchandise.dto";
import { LogService } from "@/log/log.service";
import { LogType } from "@/constants";
@Injectable()
export class MerchandiseService {
	private readonly logger = new Logger(MerchandiseService.name);

	constructor(private readonly configService: ConfigService, private readonly logService: LogService) { }

	private readonly request = new HttpRequestUtils();

	private generateConfigOption() {
		const configOption: ConfigTokenRequestParam = {
			url: this.configService.get<string>('ID_SERVICE_HOST') + '/token',
			method: 'POST',
			clientName: 'authentication-service',
			secret: this.configService.get<string>('ID_SERVICE_SECRET'),
		};
		return configOption;
	}

	async getAllCoupons(mobile: string): Promise<any> {
		const configOption = this.generateConfigOption();
		const options: RequestParams = {
			url: this.configService.get<string>('TCL_PROMOTION_SERVICE_HOST') + `/coupon?mobile=${mobile}&offset=0&limit=100`,
			method: 'GET'
		};

		const request = await this.request.requestInternal(configOption, options);

		if (!(request?.data?.length > 0)) {
			this.logger.error(`request data is empty`);
			return null;
		}
		//TODO: check if merchandise status is ACTIVE, REDEEMED, EXPIRED
		request.data = request.data.filter((item: any) => item.posIdentifier === 'merchandise').map(({ code, name, images, offerId, brandCode, startTime, endTime }) => ({ voucherCode: code, rewardName: name, images, offerId, brandCode, startTime, endTime }));
		return request;
	}

	async validateCoupon(mobile: string, code: string, brandCode: string): Promise<any> {
		const configOption = this.generateConfigOption();
		const options: RequestParams = {
			url: this.configService.get<string>('TCL_PROMOTION_SERVICE_HOST') + '/coupon/action',
			method: 'PUT',
			data: {
				action: 'validate',
				data: {
					'code': code,
					'mobile': mobile,
					'storeCode': this.configService.get<string>('STORE_CODE'),
					'billNumber': brandCode + code,
					'brandCode': brandCode,
				}
			}
		};
		const request = await this.request.requestInternal(configOption, options);
		return request;
	}

	async redeemCoupon(mobile: string, merchandise: MerchandiseDto): Promise<any> {
		const configOption = this.generateConfigOption();
		const options: RequestParams = {
			url: this.configService.get<string>('TCL_PROMOTION_SERVICE_HOST') + '/coupon/action',
			method: 'PUT',
			data: {
				action: 'redeem',
				data: {
					'code': merchandise.voucherCode,
					'mobile': mobile,
					'storeCode': this.configService.get<string>('STORE_CODE'),
					'billNumber': merchandise.brandCode + merchandise.voucherCode,
					'brandCode': merchandise.brandCode,
				}
			}
		};

		const request = await this.request.requestInternal(configOption, options);
		return request;
	}

	async redeemCoupons(mobile: string, listMerchandise: MerchandiseDto[]): Promise<any> {
		for (const merchandise of listMerchandise) {
			const validate = await this.validateCoupon(mobile, merchandise.voucherCode, merchandise.brandCode);
			if (!validate?.status?.success) {
				this.logService.create({
					message: 'Validate coupon failed for coupon ' + merchandise.voucherCode + ' of mobile ' + mobile,
					type: LogType.ERROR,
				})
				return validate;
			}
		}
		for (const merchandise of listMerchandise) {
			await this.redeemCoupon(mobile, merchandise);
			this.logService.create({
				message: 'Redeem coupon ' + merchandise.voucherCode + ' from mobile ' + mobile + ' successfully',
				type: LogType.INFO,
			})
		}
		return {
			status: {
				message: 'Success',
				code: 200,
				success: true
			},
		};
	}
}
