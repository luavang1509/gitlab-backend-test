import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { Order, OrderSchema } from './schemas/order.schema';
import { MerchandiseModule } from "src/merchandise/merchandise.module";
import { AddressModule } from "src/address/address.module";
import { CommonModule } from "@/common/common.module";
import { MediaModule } from "@/media/media.module";
import { ExportHistoryModule } from "@/export-history/export-history.module";
import { LogModule } from "@/log/log.module";

@Module({
	imports: [
		MerchandiseModule,
		AddressModule,
		CommonModule,
		MediaModule,
		OrderModule,
		ExportHistoryModule,
		LogModule,
		MongooseModule.forFeature([
			{ name: Order.name, schema: OrderSchema },
		]),
	],
	controllers: [OrderController],
	providers: [OrderService],
})
export class OrderModule { }