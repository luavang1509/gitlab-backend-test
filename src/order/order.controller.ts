import { AdminAuthGuard } from "@/guards/admin-auth.guard";
import { UserAuthGuard } from "@/guards/user-auth.guard";
import { Query, Body, Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { CreateMultipleOrderDto } from "./dtos/create-multiple-order.dto";
import { OrderService } from './order.service';
@Controller('order')
export class OrderController {
	constructor(private readonly orderService: OrderService) { }

	@Get()
	@UseGuards(AdminAuthGuard)
	getOrder(@Query() query: any) {
		return this.orderService.findAll(query);
	}

	@Get('/my-order')
	@UseGuards(UserAuthGuard)
	getOrderOfUserId(@Request() req) {
		return this.orderService.findByUserId(req.user.userId);
	}

	@Post()
	@UseGuards(UserAuthGuard)
	createMultipleOrders(@Body() body: CreateMultipleOrderDto, @Request() req) {
		const temp = {
			...body,
			userId: req.user.userId,
			ownerMobile: req.user.mobile,
		}
		return this.orderService.create(temp);
	}

	@UseGuards(AdminAuthGuard)
	@Get('csv')
	getOrderCsv(@Query() query: any) {
		return this.orderService.exportCsv(query);
	}
}
