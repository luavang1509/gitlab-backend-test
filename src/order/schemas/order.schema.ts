import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { AddressDetailDto } from "@/address/dtos/address-detail.dto";
import { OrderStatus } from "src/constants";
import { MerchandiseDto } from "src/merchandise/dtos/merchandise.dto";

export type OrderDocument = Order & Document;
@Schema({ timestamps: true, collection: 'orders' })

export class Order {
	_id: string;

	@Prop({ type: String, required: true })
	userId: string;

	@Prop({ type: String, required: true })
	ownerMobile: string;

	@Prop({ type: Date, required: true })
	submitDate: Date;

	@Prop({ enum: MerchandiseDto })
	merchandise: MerchandiseDto;

	@Prop({ enum: AddressDetailDto })
	receiverAddress: AddressDetailDto;

	@Prop({ type: String, enum: OrderStatus, default: OrderStatus.NEW })
	status: OrderStatus;

	@Prop({ type: Date })
	deliveryDate: Date;

	@Prop({ type: String, required: true })
	note: string;

	@Prop()
	createdAt?: Date;

	@Prop()
	updatedAt?: Date;
}

export const OrderSchema = SchemaFactory.createForClass(Order);

