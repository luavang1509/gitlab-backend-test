import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { get } from 'lodash';
import { Order, OrderDocument } from './schemas/order.schema';
import { sanitizePageSize } from "@/utils";
import * as moment from "moment";
import { AddressService } from "src/address/address.service";
import { MerchandiseService } from "src/merchandise/merchandise.service";
import { CreateMultipleOrderDto } from "./dtos/create-multiple-order.dto";
import { Parser } from 'json2csv';
import { LogType, OrderStatus } from "src/constants";
import { MediaService } from "@/media/media.service";
import { ExportHistoryService } from "@/export-history/export-history.service";
import { LogService } from "@/log/log.service";
@Injectable()
export class OrderService {

	constructor(@InjectModel(Order.name) private readonly orderModel: Model<OrderDocument>, private readonly addressService: AddressService, private readonly merchandiseService: MerchandiseService, private readonly mediaService: MediaService, private readonly exportHistoryService: ExportHistoryService, private readonly logService: LogService) { }


	async findAll(query: any): Promise<any> {
		const _size = Number(get(query, 'size', 10));
		const _page = Number(get(query, 'page', 1));
		const { limit, skip } = sanitizePageSize(_page, _size);
		const { size, page, submitStartDate, submitEndDate, deliveryStartDate, deliveryEndDate, ...remain } = query;
		const _query = {};
		if (submitStartDate || submitEndDate) {
			const queryDate: any = {};
			if (submitStartDate) queryDate['$gte'] = moment(submitStartDate).startOf('days').toDate();
			if (submitEndDate) queryDate['$lte'] = moment(submitEndDate).endOf('days').toDate();
			_query['submitDate'] = queryDate;
		}
		if (deliveryStartDate || deliveryEndDate) {
			const queryDate: any = {};
			if (deliveryStartDate) queryDate['$gte'] = moment(deliveryStartDate).startOf('days').toDate();
			if (deliveryEndDate) queryDate['$lte'] = moment(deliveryEndDate).endOf('days').toDate();
			_query['deliveryDate'] = queryDate;
		}
		Object.keys(remain).forEach(key => {
			const merchandiseFields = ['voucherCode', 'rewardName', 'brandCode', 'offerId'];
			if (merchandiseFields.includes(key)) {
				const _value = get(query, key, '');
				if (_value) _query[`merchandise.${key}`] = {
					$regex: _value,
					$options: 'i'
				}
			}
			else {
				const _value = get(query, key, '');
				if (_value) _query[key] = {
					$regex: _value,
					$options: 'i'
				}
			}
		});
		const [total, data] = await Promise.all([
			this.orderModel.count(_query),
			this.orderModel.find(_query).limit(limit).skip(skip).sort({ createdAt: -1 }).lean(),
		]);
		const fields = ['_id', 'submitDate', 'merchandise.voucherCode', 'merchandise.rewardName', 'merchandise.offerId', 'merchandise.brandCode', 'ownerMobile', 'receiverAddress.address', 'receiverAddress.province', 'receiverAddress.district', 'receiverAddress.fullname', 'receiverAddress.mobile', 'status', 'deliveryDate', 'note'];
		const newFields = fields.map(field => field.split('.').pop());
		const newData = data.map(item => {
			const newItem = {};
			for (let i = 0; i < fields.length; i++) {
				newItem[newFields[i]] = get(item, fields[i], '');
			}
			return newItem;
		});
		return {
			data: newData,
			meta: {
				currentPage: +_page,
				pageSize: +_size,
				totalPages: Math.ceil(total / _size),
				totalRows: total,
			},
		};
	}

	async findByUserId(userId: string) {
		const orders = await this.orderModel.find({ userId }).sort({ createdAt: -1 }).lean();
		const result = [];
		let temp = [];
		for (let i = 0; i < orders.length; i++) {
			if (i === 0) {
				temp.push(orders[i]);
			}
			else {
				if (orders[i].submitDate.getTime() === orders[i - 1].submitDate.getTime()) {
					temp.push(orders[i]);
				}
				else {
					result.push(temp);
					temp = [];
					temp.push(orders[i]);
				}
			}
		}
		if (temp.length) result.push(temp);
		return result;
	}

	async create(order: CreateMultipleOrderDto) {
		const result = await this.merchandiseService.redeemCoupons(order.ownerMobile, order.listMerchandise);
		order.submitDate = new Date();
		//TODO: CHECK IF MERCHANDISE BELONG TO USERID

		if (result.status.success) {
			for (const merchandise of order.listMerchandise) {
				const _order = { ...order, merchandise };
				delete _order.listMerchandise;
				await this.orderModel.create(_order);
			}
			this.logService.create({
				message: `Create ${order.listMerchandise.length} orders for user ${order.ownerMobile}`,
				type: LogType.INFO,
			});
			await this.addressService.updateAddress(order.userId, order.receiverAddress);
		}

		return result;
	}

	async exportCsv(query: any): Promise<any> {
		const deliveryDate = new Date();
		const { delivered, ...remain } = query;
		const contentsToExport = await this.findAll({ ...remain, size: 1000000 });
		try {
			if (delivered === 'true') {
				const ids = contentsToExport.data.map(item => item._id);
				await this.orderModel.updateMany({ _id: { $in: ids } }, { status: OrderStatus.DELIVERED, deliveryDate });
				contentsToExport.data.forEach(item => {
					item.status = OrderStatus.DELIVERED;
					item.deliveryDate = new Date();
				});
			}
			const json2csvParser = new Parser();
			const csvContent = json2csvParser.parse(contentsToExport.data);
			if (delivered === 'true') {
				const uploadedFile = await this.mediaService.uploadCsvContentToS3(csvContent);
				if (uploadedFile) await this.exportHistoryService.create({ deliveryDate, totalRows: contentsToExport.data.length, url: uploadedFile.url });
			}
			this.logService.create({
				message: `Return csv file with ${contentsToExport.data.length} rows (Upload to S3: ${delivered === 'true'})`,
				type: LogType.INFO,
			});
			return csvContent;
		} catch (err) {
			console.error(err);
		}
	}

	async updateStatus(id: string, status: OrderStatus) {
		const _order = await this.orderModel.findById(id);
		if (!_order) {
			this.logService.create({
				message: `Update status of order ${id} to ${status} failed`,
				type: LogType.ERROR,
			});
			throw new HttpException('Order not found', HttpStatus.NOT_FOUND);
		}
		_order.status = status;
		await _order.save();
		this.logService.create({
			message: `Update status of order ${id} to ${status}`,
			type: LogType.INFO,
		});
		return _order;
	}
}