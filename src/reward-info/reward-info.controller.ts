import { AdminAuthGuard } from "@/guards/admin-auth.guard";
import { UserAuthGuard } from "@/guards/user-auth.guard";
import { Query, Body, Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { RewardInfoService } from './reward-info.service';
@Controller('rewardinfo')
export class RewardInfoController {
	constructor(private readonly rewardInfoService: RewardInfoService) { }

	@Get()
	@UseGuards(AdminAuthGuard)
	getRewardInfo(@Query() query: any) {
		return this.rewardInfoService.findAll(query);
	}
}
