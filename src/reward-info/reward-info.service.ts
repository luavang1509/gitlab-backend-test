import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { get } from 'lodash';
import { RewardInfoModel, RewardInfoDocument } from './schemas/reward-info.schema';
import { sanitizePageSize } from "@/utils";

@Injectable()
export class RewardInfoService {

	constructor(@InjectModel(RewardInfoModel.name) private readonly rewardInfoModel: Model<RewardInfoDocument>) { }
	async findAll(query: any): Promise<any> {
		const _size = Number(get(query, 'size', 10));
		const _page = Number(get(query, 'page', 1));
		const { limit, skip } = sanitizePageSize(_page, _size);
		const [total, data] = await Promise.all([
			this.rewardInfoModel.count(),
			this.rewardInfoModel.find().limit(limit).skip(skip).sort({ createdAt: -1 }).lean(),
		]);
		return {
			data,
			meta: {
				currentPage: +_page,
				pageSize: +_size,
				totalPages: Math.ceil(total / _size),
				totalRows: total,
			},
		};
	}
}