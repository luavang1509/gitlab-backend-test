import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RewardInfoController } from './reward-info.controller';
import { RewardInfoService } from './reward-info.service';
import { RewardInfoModel, RewardInfoSchema } from './schemas/reward-info.schema';
import { MerchandiseModule } from "src/merchandise/merchandise.module";
import { AddressModule } from "src/address/address.module";
import { CommonModule } from "@/common/common.module";
import { MediaModule } from "@/media/media.module";
import { ExportHistoryModule } from "@/export-history/export-history.module";
import { LogModule } from "@/log/log.module";

@Module({
	imports: [
		MerchandiseModule,
		AddressModule,
		CommonModule,
		MediaModule,
		RewardInfoModule,
		ExportHistoryModule,
		LogModule,
		MongooseModule.forFeature([
			{ name: RewardInfoModel.name, schema: RewardInfoSchema },
		]),
	],
	controllers: [RewardInfoController],
	providers: [RewardInfoService],
})
export class RewardInfoModule { }