import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type RewardInfoDocument = RewardInfoModel & Document;
@Schema({ timestamps: true, collection: 'reward-infos' })

export class RewardInfoModel {
	_id: string;

	@Prop({ type: String, required: true })
	rewardName: string;

	@Prop({ type: String, required: true })
	imageUrl: string;
}

export const RewardInfoSchema = SchemaFactory.createForClass(RewardInfoModel);

