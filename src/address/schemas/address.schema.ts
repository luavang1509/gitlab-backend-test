import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { AddressDetailDto } from "../dtos/address-detail.dto";

export type AddressDocument = Address & Document;
@Schema({
	timestamps: true,
	collection: 'addresses',
})

export class Address {
	_id: string;

	@Prop({ type: String, index: true, unique: true, required: true, trim: true })
	userId: string;

	@Prop({ enum: AddressDetailDto })
	addressDetail: AddressDetailDto;
}

export const AddressSchema = SchemaFactory.createForClass(Address);

