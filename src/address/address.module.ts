import { CommonModule } from "@/common/common.module";
import { LogModule } from "@/log/log.module";
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AddressController } from './address.controller';
import { AddressService } from './address.service';
import { Address, AddressSchema } from './schemas/address.schema';

@Module({
	imports: [
		MongooseModule.forFeature([
			{ name: Address.name, schema: AddressSchema },
		]),
		LogModule,
		CommonModule
	],
	controllers: [AddressController],
	providers: [AddressService],
	exports: [AddressService],
})
export class AddressModule { }