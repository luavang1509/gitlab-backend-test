import { Query, Body, Controller, Delete, Get, Param, Post, Put, UseGuards, Request } from '@nestjs/common';
import { AddressDetailDto } from "./dtos/address-detail.dto";
import { AddressService } from './address.service';
import { Logger } from "@nestjs/common/services";
import { UserAuthGuard } from "@/guards/user-auth.guard";

@Controller('address')
export class AddressController {
	constructor(private readonly addressService: AddressService) { }

	@UseGuards(UserAuthGuard)
	@Get('my-address')
	getAddress(@Request() req) {
		return this.addressService.getAddress(req.user.userId)
	}
}
