import { IsNotEmpty, IsString } from 'class-validator';

export class AddressDetailDto {
	@IsString()
	@IsNotEmpty()
	address: string;

	@IsString()
	@IsNotEmpty()
	province: string;

	@IsString()
	@IsNotEmpty()
	district: string;

	@IsString()
	@IsNotEmpty()
	mobile: string;

	@IsString()
	@IsNotEmpty()
	fullname: string;
}
