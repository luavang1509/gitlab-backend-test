import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { get } from 'lodash';
import { Address, AddressDocument } from './schemas/address.schema';
import { AddressDetailDto } from "./dtos/address-detail.dto";
import { LogService } from "@/log/log.service";
import { LogType } from "@/constants";

@Injectable()
export class AddressService {
	constructor(@InjectModel(Address.name) private readonly addressModel: Model<AddressDocument>, private readonly logService: LogService) { }

	async getAddress(userId: string) {
		const address = await this.addressModel.findOne({ userId });
		if (address)
			return address;
		return {}
	}

	async updateAddress(userId: string, addressDetail: AddressDetailDto) {
		const address = await this.addressModel.findOneAndUpdate({ userId }, { addressDetail }, { upsert: true, new: true });
		this.logService.create({
			message: `Update address for user ${userId}`,
			type: LogType.INFO,
		});
		return address;
	}
}