import { ADMIN_ACCESS_TOKEN_PREFIX } from '@/constants';
import {
	CanActivate,
	ExecutionContext,
	ForbiddenException,
	Injectable
} from '@nestjs/common';
import { RedisService } from "../common/services/redis.service";
import { getBearerToken } from '@/utils';

@Injectable()
export class AdminAuthGuard implements CanActivate {
	constructor(private redisService: RedisService) { }

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const request = context.switchToHttp().getRequest();
		const header = request.header('Authorization');
		const token = getBearerToken(header);
		const user = await this.validateToken(token);

		if (user) {
			request.user = user;
			return true;
		}
		throw new ForbiddenException();
	}

	async validateToken(token: string) {
		if (token) {
			const key = `${ADMIN_ACCESS_TOKEN_PREFIX}.${token}`;
			const data = await this.redisService.get(key);

			if (data) {
				const user = JSON.parse(data);
				return user;
			}
		}
		return null;
	}
}
