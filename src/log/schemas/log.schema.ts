import { LogType } from "@/constants";
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LogDocument = LogModel & Document;
@Schema({ timestamps: true, collection: 'log-histories' })

export class LogModel {
	_id: string;

	@Prop({ type: String })
	action: string;

	@Prop({ type: String, required: true })
	message: string;

	@Prop({ type: String, enum: LogType })
	type: LogType;
}

export const LogSchema = SchemaFactory.createForClass(LogModel);

