import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { get } from 'lodash';
import { LogModel, LogDocument } from './schemas/log.schema';
import { sanitizePageSize } from "@/utils";
@Injectable()
export class LogService {
	constructor(@InjectModel(LogModel.name) private readonly logModel: Model<LogDocument>) { }
	async findAll(query: any) {
		const _size = Number(get(query, 'size', 10));
		const _page = Number(get(query, 'page', 1));
		const { limit, skip } = sanitizePageSize(_page, _size);
		const [total, data] = await Promise.all([
			this.logModel.count(),
			this.logModel.find().limit(limit).skip(skip).sort({ createdAt: -1 }).lean(),
		]);
		return {
			data,
			meta: {
				currentPage: +_page,
				pageSize: +_size,
				totalPages: Math.ceil(total / _size),
				totalRows: total,
			},
		};
	}
	async create(data: any) {
		return this.logModel.create(data);
	}
}