import { AdminAuthGuard } from "@/guards/admin-auth.guard";
import { Query, Body, Controller, Get, Post, UseGuards, Param } from '@nestjs/common';
import { LogService } from './log.service';
@Controller('log')
export class LogController {
	constructor(private readonly logService: LogService) { }

	@Get()
	@UseGuards(AdminAuthGuard)
	getlog(@Query('page') page: number, @Query('size') size: number) {
		return this.logService.findAll({ page, size });
	}
}
