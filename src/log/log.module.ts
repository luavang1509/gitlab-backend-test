import { CommonModule } from "@/common/common.module";
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LogController } from './log.controller';
import { LogService } from './log.service';
import { LogModel, LogSchema } from './schemas/log.schema';

@Module({
	imports: [
		MongooseModule.forFeature([
			{ name: LogModel.name, schema: LogSchema },
		]),
		CommonModule
	],
	controllers: [LogController],
	providers: [LogService],
	exports: [LogService],
})
export class LogModule { }