import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import * as cookieParser from 'cookie-parser';
import { ApiLogger } from '@taptap-discovery/api-logger';
import { AppModule } from './app.module';

// import * as fs from 'fs';

import 'dotenv/config';
import { LogLevel, ValidationPipe } from "@nestjs/common";

// const httpsOptions = {
//   key: fs.readFileSync('./storage/https.key'),
//   cert: fs.readFileSync('./storage/https.crt'),
// };

async function bootstrap() {
	const logger: LogLevel[] = ['log', 'error', 'warn', 'debug', 'verbose'];
	const app = await NestFactory.create(AppModule, { logger });

	// Set config
	const configService: ConfigService = app.get(ConfigService);
	app.enableCors();
	app.use(cookieParser());
	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true,
		}),
	);
	if (configService.get<string>('api_logger') == 'true') {
		const apiLogger = new ApiLogger();
		app.use(apiLogger.httpLogger);
	}
	const port = configService.get('port');
	await app.listen(port);
	console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
