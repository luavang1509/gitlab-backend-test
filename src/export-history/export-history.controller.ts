import { AdminAuthGuard } from "@/guards/admin-auth.guard";
import { Query, Controller, Get, UseGuards } from '@nestjs/common';
import { ExportHistoryService } from './export-history.service';
@Controller('export-history')
export class ExportHistoryController {
	constructor(private readonly exportHistoryService: ExportHistoryService) { }

	@Get()
	@UseGuards(AdminAuthGuard)
	getExportHistory(@Query() query: any) {
		return this.exportHistoryService.findAll(query);
	}

	@Get(':id')
	@UseGuards(AdminAuthGuard)
	getExportHistoryById(@Query() query: any) {
		return this.exportHistoryService.findById(query);
	}
}
