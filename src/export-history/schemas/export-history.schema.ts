import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ExportHistoryDocument = ExportHistoryModel & Document;

@Schema({ timestamps: true, collection: 'export-history' })
export class ExportHistoryModel {
	@Prop({ type: Date, required: true })
	deliveryDate: Date;
	@Prop({ type: Number, required: true })
	totalRows: Number;
	@Prop({ type: String, required: true, trim: true })
	url: string;
}

export const ExportHistorySchema = SchemaFactory.createForClass(ExportHistoryModel);
