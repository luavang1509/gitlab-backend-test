import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ExportHistoryController } from './export-history.controller';
import { ExportHistoryService } from './export-history.service';
import { ExportHistoryModel, ExportHistorySchema } from './schemas/export-history.schema';
import { MerchandiseModule } from "src/merchandise/merchandise.module";
import { AddressModule } from "src/address/address.module";
import { CommonModule } from "@/common/common.module";
import { LogModule } from "@/log/log.module";

@Module({
	imports: [
		MerchandiseModule,
		AddressModule,
		CommonModule,
		LogModule,
		MongooseModule.forFeature([
			{ name: ExportHistoryModel.name, schema: ExportHistorySchema },
		]),
	],
	controllers: [ExportHistoryController],
	providers: [ExportHistoryService],
	exports: [ExportHistoryService],
})
export class ExportHistoryModule { }