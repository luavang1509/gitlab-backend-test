import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { get } from 'lodash';
import { sanitizePageSize } from "@/utils";
import * as moment from "moment";
import { ExportHistoryDocument, ExportHistoryModel } from "./schemas/export-history.schema";
import { ExportHistoryDto } from "./dtos/export-history.dto";
import { LogService } from "@/log/log.service";
import { LogType } from "@/constants";
@Injectable()
export class ExportHistoryService {

	constructor(@InjectModel(ExportHistoryModel.name) private readonly exportHistoryModel: Model<ExportHistoryDocument>, private readonly logService: LogService) { }


	async findAll(query: any): Promise<any> {
		const _size = Number(get(query, 'size', 10));
		const _page = Number(get(query, 'page', 1));
		const { limit, skip } = sanitizePageSize(_page, _size);
		const [total, data] = await Promise.all([
			this.exportHistoryModel.count(),
			this.exportHistoryModel.find().limit(limit).skip(skip).sort({ createdAt: -1 }).lean(),
		]);
		return {
			data,
			meta: {
				currentPage: +_page,
				pageSize: +_size,
				totalPages: Math.ceil(total / _size),
				totalRows: total,
			},
		};
	}

	async findById(query: any): Promise<any> {
		const { id } = query;
		const data = await this.exportHistoryModel.findById(id).lean();
		return {
			data,
		};
	}

	async create(data: ExportHistoryDto): Promise<any> {
		this.logService.create({
			message: `Create export history with rows = ${data.totalRows}, url = ${data.url}`,
			type: LogType.INFO,
		});
		return await this.exportHistoryModel.create(data);
	}
}