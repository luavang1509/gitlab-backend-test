import { IsDate, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class ExportHistoryDto {
	@IsDate()
	@IsNotEmpty()
	deliveryDate: Date;

	@IsNumber()
	@IsNotEmpty()
	totalRows: Number;

	@IsString()
	@IsNotEmpty()
	url: string;
}
