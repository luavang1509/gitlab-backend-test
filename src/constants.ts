export enum Status {
	ISSUED = 'Issued',
	REDEEMED = 'Redeemed',
	EXPIRED = 'Expired',
}

export enum OrderStatus {
	NEW = 'New',
	DELIVERED = 'Delivered',
	CANCELLED = 'Cancelled',
}

export enum LogType {
	INFO = 'Info',
	WARNING = 'Warning',
	ERROR = 'Error',
}

export const ADMIN_ACCESS_TOKEN_PREFIX = 'cmsadmin.access.token';
export const USER_ACCESS_TOKEN_PREFIX = 'cmsuser.access.token';