import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from '../schemas/user.schema';

@Injectable()
export class UserService {
	constructor(
		@InjectModel(User.name)
		private userModel: Model<UserDocument>,
	) { }

	findAll(filter) {
		return this.userModel.find(filter, { password: 0 });
	}

	findById(id: string) {
		return this.userModel.findById(id, { password: 0 });
	}

	findByUsername(username: string) {
		return this.userModel.findOne({ username });
	}

	create(dto: any) {
		return this.userModel.create(dto);
	}

	update(id: string, dto: any) {
		return this.userModel.findByIdAndUpdate(id, dto, { new: true });
	}

	delete(id: string) {
		return this.userModel.findByIdAndDelete(id);
	}
}
