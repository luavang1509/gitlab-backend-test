"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getExtFromFileName = exports.getMimeFromFileName = exports.parsePageNum = exports.generateFileName = exports.findRangeOfNumber = exports.arrayShuffle = exports.sanitizePageSize = exports.randomInt = exports.getFileExt = exports.validatePassword = exports.hashPassword = exports.getBearerToken = exports.generateUUIDWithMoment = exports.generateUUID = exports.generateToken = exports.md5 = exports.sha256 = void 0;
const common_1 = require("@nestjs/common");
const short_unique_id_1 = require("short-unique-id");
const crypto = require("crypto");
const moment = require("moment");
const path = require("path");
const ShortID12 = new short_unique_id_1.default({ length: 12 });
const sha256 = (input) => {
    return crypto.createHash('sha256').update(input).digest('hex');
};
exports.sha256 = sha256;
const md5 = (input) => {
    return crypto.createHash('md5').update(input).digest('hex');
};
exports.md5 = md5;
const generateToken = () => {
    return crypto.randomBytes(32).toString('hex');
};
exports.generateToken = generateToken;
const generateUUID = () => {
    return crypto.randomUUID();
};
exports.generateUUID = generateUUID;
const generateUUIDWithMoment = () => {
    const now = moment().unix();
    return `${now}${ShortID12.randomUUID()}`;
};
exports.generateUUIDWithMoment = generateUUIDWithMoment;
const getBearerToken = (header) => {
    return typeof header === 'string'
        ? header.replace(/^Bearer /, '').trim()
        : '';
};
exports.getBearerToken = getBearerToken;
const hashPassword = (input) => {
    return (0, exports.md5)(input);
};
exports.hashPassword = hashPassword;
const validatePassword = (input, hash) => {
    return (0, exports.hashPassword)(input) === hash;
};
exports.validatePassword = validatePassword;
const getFileExt = function (filename) {
    const ext = path.extname(filename);
    let normalize = ext;
    if (normalize) {
        normalize = normalize.toLowerCase();
        if ('.jpeg' === normalize) {
            normalize = '.jpg';
        }
    }
    return {
        ext: ext,
        normalize: normalize,
    };
};
exports.getFileExt = getFileExt;
function randomInt(min, max) {
    const rand = Math.random() * (max - min);
    return Math.floor(rand + min);
}
exports.randomInt = randomInt;
const sanitizePageSize = function (page, size) {
    if (page < 1 || size < 0)
        throw new common_1.HttpException('error page size', 400);
    const limit = size || 10;
    const skip = (page - 1) * size;
    return {
        limit,
        skip,
    };
};
exports.sanitizePageSize = sanitizePageSize;
const arrayShuffle = (array) => {
    for (let i = 0, length = array.length, swap = 0, temp = ''; i < length; i++) {
        swap = Math.floor(Math.random() * (i + 1));
        temp = array[swap];
        array[swap] = array[i];
        array[i] = temp;
    }
    return array;
};
exports.arrayShuffle = arrayShuffle;
const findRangeOfNumber = (rangeArray, number) => {
    for (let i = 0; i < rangeArray.length; i++) {
        if (number <= Number(rangeArray[i])) {
            return rangeArray[i];
        }
    }
    return rangeArray[rangeArray.length - 1];
};
exports.findRangeOfNumber = findRangeOfNumber;
const generateFileName = (ext) => {
    const name = (0, exports.generateUUIDWithMoment)();
    return crypto.createHash('md5').update(name).digest('hex') + ext;
};
exports.generateFileName = generateFileName;
const parsePageNum = (v) => {
    if (v === null || v === undefined) {
        return 1;
    }
    if (isNaN(v)) {
        return 1;
    }
    return Math.max(parseInt(v), 1);
};
exports.parsePageNum = parsePageNum;
const getMimeFromFileName = (filename) => {
    const ext = (0, exports.getExtFromFileName)(filename);
    switch (ext) {
        case '.jpg':
            return 'image/jpeg';
        case '.png':
            return 'image/png';
        case '.csv':
            return 'text/csv';
        default:
            return 'application/octet-stream';
    }
};
exports.getMimeFromFileName = getMimeFromFileName;
const getExtFromFileName = (filename) => {
    let ext = path.extname(filename).toLowerCase();
    if (ext == '.jpeg')
        ext = '.jpg';
    return ext;
};
exports.getExtFromFileName = getExtFromFileName;
//# sourceMappingURL=utils.js.map