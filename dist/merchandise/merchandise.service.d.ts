import { ConfigService } from "@nestjs/config";
import { MerchandiseDto } from "./dtos/merchandise.dto";
import { LogService } from "@/log/log.service";
export declare class MerchandiseService {
    private readonly configService;
    private readonly logService;
    private readonly logger;
    constructor(configService: ConfigService, logService: LogService);
    private readonly request;
    private generateConfigOption;
    getAllCoupons(mobile: string): Promise<any>;
    validateCoupon(mobile: string, code: string, brandCode: string): Promise<any>;
    redeemCoupon(mobile: string, merchandise: MerchandiseDto): Promise<any>;
    redeemCoupons(mobile: string, listMerchandise: MerchandiseDto[]): Promise<any>;
}
