"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var MerchandiseService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchandiseService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const http_request_utils_1 = require("@taptap-discovery/http-request-utils");
const log_service_1 = require("../log/log.service");
const constants_1 = require("../constants");
let MerchandiseService = MerchandiseService_1 = class MerchandiseService {
    constructor(configService, logService) {
        this.configService = configService;
        this.logService = logService;
        this.logger = new common_1.Logger(MerchandiseService_1.name);
        this.request = new http_request_utils_1.HttpRequestUtils();
    }
    generateConfigOption() {
        const configOption = {
            url: this.configService.get('ID_SERVICE_HOST') + '/token',
            method: 'POST',
            clientName: 'authentication-service',
            secret: this.configService.get('ID_SERVICE_SECRET'),
        };
        return configOption;
    }
    async getAllCoupons(mobile) {
        var _a;
        const configOption = this.generateConfigOption();
        const options = {
            url: this.configService.get('TCL_PROMOTION_SERVICE_HOST') + `/coupon?mobile=${mobile}&offset=0&limit=100`,
            method: 'GET'
        };
        const request = await this.request.requestInternal(configOption, options);
        if (!(((_a = request === null || request === void 0 ? void 0 : request.data) === null || _a === void 0 ? void 0 : _a.length) > 0)) {
            this.logger.error(`request data is empty`);
            return null;
        }
        request.data = request.data.filter((item) => item.posIdentifier === 'merchandise').map(({ code, name, images, offerId, brandCode, startTime, endTime }) => ({ voucherCode: code, rewardName: name, images, offerId, brandCode, startTime, endTime }));
        return request;
    }
    async validateCoupon(mobile, code, brandCode) {
        const configOption = this.generateConfigOption();
        const options = {
            url: this.configService.get('TCL_PROMOTION_SERVICE_HOST') + '/coupon/action',
            method: 'PUT',
            data: {
                action: 'validate',
                data: {
                    'code': code,
                    'mobile': mobile,
                    'storeCode': this.configService.get('STORE_CODE'),
                    'billNumber': brandCode + code,
                    'brandCode': brandCode,
                }
            }
        };
        const request = await this.request.requestInternal(configOption, options);
        return request;
    }
    async redeemCoupon(mobile, merchandise) {
        const configOption = this.generateConfigOption();
        const options = {
            url: this.configService.get('TCL_PROMOTION_SERVICE_HOST') + '/coupon/action',
            method: 'PUT',
            data: {
                action: 'redeem',
                data: {
                    'code': merchandise.voucherCode,
                    'mobile': mobile,
                    'storeCode': this.configService.get('STORE_CODE'),
                    'billNumber': merchandise.brandCode + merchandise.voucherCode,
                    'brandCode': merchandise.brandCode,
                }
            }
        };
        const request = await this.request.requestInternal(configOption, options);
        return request;
    }
    async redeemCoupons(mobile, listMerchandise) {
        var _a;
        for (const merchandise of listMerchandise) {
            const validate = await this.validateCoupon(mobile, merchandise.voucherCode, merchandise.brandCode);
            if (!((_a = validate === null || validate === void 0 ? void 0 : validate.status) === null || _a === void 0 ? void 0 : _a.success)) {
                this.logService.create({
                    message: 'Validate coupon failed for coupon ' + merchandise.voucherCode + ' of mobile ' + mobile,
                    type: constants_1.LogType.ERROR,
                });
                return validate;
            }
        }
        for (const merchandise of listMerchandise) {
            await this.redeemCoupon(mobile, merchandise);
            this.logService.create({
                message: 'Redeem coupon ' + merchandise.voucherCode + ' from mobile ' + mobile + ' successfully',
                type: constants_1.LogType.INFO,
            });
        }
        return {
            status: {
                message: 'Success',
                code: 200,
                success: true
            },
        };
    }
};
MerchandiseService = MerchandiseService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService, log_service_1.LogService])
], MerchandiseService);
exports.MerchandiseService = MerchandiseService;
//# sourceMappingURL=merchandise.service.js.map