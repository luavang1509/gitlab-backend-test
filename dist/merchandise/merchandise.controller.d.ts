import { MerchandiseService } from './merchandise.service';
export declare class MerchandiseController {
    private readonly merchandiseService;
    constructor(merchandiseService: MerchandiseService);
    getMerchandiseByUserId(req: any): Promise<any>;
}
