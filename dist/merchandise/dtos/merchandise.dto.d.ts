export declare class MerchandiseDto {
    voucherCode: string;
    rewardName: string;
    brandCode: string;
    offerId: string;
}
