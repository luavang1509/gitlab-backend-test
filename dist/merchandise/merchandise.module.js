"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchandiseModule = void 0;
const auth_module_1 = require("../auth/auth.module");
const common_module_1 = require("../common/common.module");
const log_module_1 = require("../log/log.module");
const common_1 = require("@nestjs/common");
const merchandise_controller_1 = require("./merchandise.controller");
const merchandise_service_1 = require("./merchandise.service");
let MerchandiseModule = class MerchandiseModule {
};
MerchandiseModule = __decorate([
    (0, common_1.Module)({
        imports: [auth_module_1.AuthModule, common_module_1.CommonModule, log_module_1.LogModule],
        controllers: [merchandise_controller_1.MerchandiseController],
        providers: [merchandise_service_1.MerchandiseService],
        exports: [merchandise_service_1.MerchandiseService],
    })
], MerchandiseModule);
exports.MerchandiseModule = MerchandiseModule;
//# sourceMappingURL=merchandise.module.js.map