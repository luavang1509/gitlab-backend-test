"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExportHistoryController = void 0;
const admin_auth_guard_1 = require("../guards/admin-auth.guard");
const common_1 = require("@nestjs/common");
const export_history_service_1 = require("./export-history.service");
let ExportHistoryController = class ExportHistoryController {
    constructor(exportHistoryService) {
        this.exportHistoryService = exportHistoryService;
    }
    getExportHistory(query) {
        return this.exportHistoryService.findAll(query);
    }
    getExportHistoryById(query) {
        return this.exportHistoryService.findById(query);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ExportHistoryController.prototype, "getExportHistory", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ExportHistoryController.prototype, "getExportHistoryById", null);
ExportHistoryController = __decorate([
    (0, common_1.Controller)('export-history'),
    __metadata("design:paramtypes", [export_history_service_1.ExportHistoryService])
], ExportHistoryController);
exports.ExportHistoryController = ExportHistoryController;
//# sourceMappingURL=export-history.controller.js.map