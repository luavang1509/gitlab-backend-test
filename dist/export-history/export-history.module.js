"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExportHistoryModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const export_history_controller_1 = require("./export-history.controller");
const export_history_service_1 = require("./export-history.service");
const export_history_schema_1 = require("./schemas/export-history.schema");
const merchandise_module_1 = require("../merchandise/merchandise.module");
const address_module_1 = require("../address/address.module");
const common_module_1 = require("../common/common.module");
const log_module_1 = require("../log/log.module");
let ExportHistoryModule = class ExportHistoryModule {
};
ExportHistoryModule = __decorate([
    (0, common_1.Module)({
        imports: [
            merchandise_module_1.MerchandiseModule,
            address_module_1.AddressModule,
            common_module_1.CommonModule,
            log_module_1.LogModule,
            mongoose_1.MongooseModule.forFeature([
                { name: export_history_schema_1.ExportHistoryModel.name, schema: export_history_schema_1.ExportHistorySchema },
            ]),
        ],
        controllers: [export_history_controller_1.ExportHistoryController],
        providers: [export_history_service_1.ExportHistoryService],
        exports: [export_history_service_1.ExportHistoryService],
    })
], ExportHistoryModule);
exports.ExportHistoryModule = ExportHistoryModule;
//# sourceMappingURL=export-history.module.js.map