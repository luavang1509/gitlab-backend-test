"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExportHistoryService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const lodash_1 = require("lodash");
const utils_1 = require("../utils");
const export_history_schema_1 = require("./schemas/export-history.schema");
const log_service_1 = require("../log/log.service");
const constants_1 = require("../constants");
let ExportHistoryService = class ExportHistoryService {
    constructor(exportHistoryModel, logService) {
        this.exportHistoryModel = exportHistoryModel;
        this.logService = logService;
    }
    async findAll(query) {
        const _size = Number((0, lodash_1.get)(query, 'size', 10));
        const _page = Number((0, lodash_1.get)(query, 'page', 1));
        const { limit, skip } = (0, utils_1.sanitizePageSize)(_page, _size);
        const [total, data] = await Promise.all([
            this.exportHistoryModel.count(),
            this.exportHistoryModel.find().limit(limit).skip(skip).sort({ createdAt: -1 }).lean(),
        ]);
        return {
            data,
            meta: {
                currentPage: +_page,
                pageSize: +_size,
                totalPages: Math.ceil(total / _size),
                totalRows: total,
            },
        };
    }
    async findById(query) {
        const { id } = query;
        const data = await this.exportHistoryModel.findById(id).lean();
        return {
            data,
        };
    }
    async create(data) {
        this.logService.create({
            message: `Create export history with rows = ${data.totalRows}, url = ${data.url}`,
            type: constants_1.LogType.INFO,
        });
        return await this.exportHistoryModel.create(data);
    }
};
ExportHistoryService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(export_history_schema_1.ExportHistoryModel.name)),
    __metadata("design:paramtypes", [mongoose_2.Model, log_service_1.LogService])
], ExportHistoryService);
exports.ExportHistoryService = ExportHistoryService;
//# sourceMappingURL=export-history.service.js.map