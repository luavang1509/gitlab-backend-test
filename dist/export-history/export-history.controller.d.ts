import { ExportHistoryService } from './export-history.service';
export declare class ExportHistoryController {
    private readonly exportHistoryService;
    constructor(exportHistoryService: ExportHistoryService);
    getExportHistory(query: any): Promise<any>;
    getExportHistoryById(query: any): Promise<any>;
}
