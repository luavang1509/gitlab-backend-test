export declare class ExportHistoryDto {
    deliveryDate: Date;
    totalRows: Number;
    url: string;
}
