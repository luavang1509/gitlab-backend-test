import { Model } from 'mongoose';
import { ExportHistoryDocument } from "./schemas/export-history.schema";
import { ExportHistoryDto } from "./dtos/export-history.dto";
import { LogService } from "@/log/log.service";
export declare class ExportHistoryService {
    private readonly exportHistoryModel;
    private readonly logService;
    constructor(exportHistoryModel: Model<ExportHistoryDocument>, logService: LogService);
    findAll(query: any): Promise<any>;
    findById(query: any): Promise<any>;
    create(data: ExportHistoryDto): Promise<any>;
}
