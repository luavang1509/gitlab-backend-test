"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const config_1 = require("@nestjs/config");
const cookieParser = require("cookie-parser");
const api_logger_1 = require("@taptap-discovery/api-logger");
const app_module_1 = require("./app.module");
require("dotenv/config");
const common_1 = require("@nestjs/common");
async function bootstrap() {
    const logger = ['log', 'error', 'warn', 'debug', 'verbose'];
    const app = await core_1.NestFactory.create(app_module_1.AppModule, { logger });
    const configService = app.get(config_1.ConfigService);
    app.enableCors();
    app.use(cookieParser());
    app.useGlobalPipes(new common_1.ValidationPipe({
        whitelist: true,
    }));
    if (configService.get('api_logger') == 'true') {
        const apiLogger = new api_logger_1.ApiLogger();
        app.use(apiLogger.httpLogger);
    }
    const port = configService.get('port');
    await app.listen(port);
    console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
//# sourceMappingURL=main.js.map