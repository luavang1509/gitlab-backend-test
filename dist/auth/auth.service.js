"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AuthService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("../dao/services/user.service");
const utils_1 = require("../utils");
const http_request_utils_1 = require("@taptap-discovery/http-request-utils");
const constants_1 = require("../constants");
const redis_service_1 = require("../common/services/redis.service");
const config_1 = require("@nestjs/config");
let AuthService = AuthService_1 = class AuthService {
    constructor(userService, redisService, configService) {
        this.userService = userService;
        this.redisService = redisService;
        this.configService = configService;
        this.logger = new common_1.Logger(AuthService_1.name);
        this.request = new http_request_utils_1.HttpRequestUtils();
    }
    async login(dto) {
        const user = await this.userService.findByUsername(dto.username);
        if (!user) {
            throw new common_1.BadRequestException('Invalid username');
        }
        if (!(0, utils_1.validatePassword)(dto.password, user.password)) {
            throw new common_1.BadRequestException('Invalid password');
        }
        return {
            _id: user._id,
            _username: user.username
        };
    }
    async generateAccessToken(user) {
        const token = (0, utils_1.generateToken)();
        const key = `${constants_1.ADMIN_ACCESS_TOKEN_PREFIX}.${token}`;
        const data = {
            _id: user._id,
            email: user.email,
            displayName: user.displayName,
            group: user.group,
        };
        const ttl = 24 * 3600 * 1000;
        await this.redisService.setEx(key, JSON.stringify(data), ttl);
        return token;
    }
    async generateUserAccessToken(user) {
        const token = (0, utils_1.generateToken)();
        const key = `${constants_1.USER_ACCESS_TOKEN_PREFIX}.${token}`;
        const data = {
            userId: user.userId,
            mobile: user.mobile,
        };
        const ttl = 24 * 3600 * 1000;
        await this.redisService.setEx(key, JSON.stringify(data), ttl);
        return token;
    }
    async removeAccessToken(token, tokenPrefix = constants_1.ADMIN_ACCESS_TOKEN_PREFIX) {
        const key = `${tokenPrefix}.${token}`;
        await this.redisService.del(key);
    }
    async loginToken(token) {
        const options = {
            url: `http://localhost:3002/verify_token?token=${token}`,
            method: 'GET'
        };
        const request = await this.request.request(options);
        return request;
    }
};
AuthService = AuthService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        redis_service_1.RedisService,
        config_1.ConfigService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map