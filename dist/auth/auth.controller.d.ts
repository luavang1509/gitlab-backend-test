import { AuthService } from './auth.service';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    login(body: any): Promise<{
        data: {
            accessToken: string;
            _id: any;
            _username: string;
        };
    }>;
    loginToken(body: any): Promise<any>;
}
