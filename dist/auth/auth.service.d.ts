import { UserService } from "@/dao/services/user.service";
import { RedisService } from "@/common/services/redis.service";
import { ConfigService } from "@nestjs/config";
export declare class AuthService {
    private userService;
    private redisService;
    private readonly configService;
    private logger;
    private readonly request;
    constructor(userService: UserService, redisService: RedisService, configService: ConfigService);
    login(dto: any): Promise<{
        _id: any;
        _username: string;
    }>;
    generateAccessToken(user: any): Promise<string>;
    generateUserAccessToken(user: any): Promise<string>;
    removeAccessToken(token: any, tokenPrefix?: string): Promise<void>;
    loginToken(token: string): Promise<any>;
}
