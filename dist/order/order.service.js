"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const lodash_1 = require("lodash");
const order_schema_1 = require("./schemas/order.schema");
const utils_1 = require("../utils");
const moment = require("moment");
const address_service_1 = require("../address/address.service");
const merchandise_service_1 = require("../merchandise/merchandise.service");
const json2csv_1 = require("json2csv");
const constants_1 = require("../constants");
const media_service_1 = require("../media/media.service");
const export_history_service_1 = require("../export-history/export-history.service");
const log_service_1 = require("../log/log.service");
let OrderService = class OrderService {
    constructor(orderModel, addressService, merchandiseService, mediaService, exportHistoryService, logService) {
        this.orderModel = orderModel;
        this.addressService = addressService;
        this.merchandiseService = merchandiseService;
        this.mediaService = mediaService;
        this.exportHistoryService = exportHistoryService;
        this.logService = logService;
    }
    async findAll(query) {
        const _size = Number((0, lodash_1.get)(query, 'size', 10));
        const _page = Number((0, lodash_1.get)(query, 'page', 1));
        const { limit, skip } = (0, utils_1.sanitizePageSize)(_page, _size);
        const { size, page, submitStartDate, submitEndDate, deliveryStartDate, deliveryEndDate } = query, remain = __rest(query, ["size", "page", "submitStartDate", "submitEndDate", "deliveryStartDate", "deliveryEndDate"]);
        const _query = {};
        if (submitStartDate || submitEndDate) {
            const queryDate = {};
            if (submitStartDate)
                queryDate['$gte'] = moment(submitStartDate).startOf('days').toDate();
            if (submitEndDate)
                queryDate['$lte'] = moment(submitEndDate).endOf('days').toDate();
            _query['submitDate'] = queryDate;
        }
        if (deliveryStartDate || deliveryEndDate) {
            const queryDate = {};
            if (deliveryStartDate)
                queryDate['$gte'] = moment(deliveryStartDate).startOf('days').toDate();
            if (deliveryEndDate)
                queryDate['$lte'] = moment(deliveryEndDate).endOf('days').toDate();
            _query['deliveryDate'] = queryDate;
        }
        Object.keys(remain).forEach(key => {
            const merchandiseFields = ['voucherCode', 'rewardName', 'brandCode', 'offerId'];
            if (merchandiseFields.includes(key)) {
                const _value = (0, lodash_1.get)(query, key, '');
                if (_value)
                    _query[`merchandise.${key}`] = {
                        $regex: _value,
                        $options: 'i'
                    };
            }
            else {
                const _value = (0, lodash_1.get)(query, key, '');
                if (_value)
                    _query[key] = {
                        $regex: _value,
                        $options: 'i'
                    };
            }
        });
        const [total, data] = await Promise.all([
            this.orderModel.count(_query),
            this.orderModel.find(_query).limit(limit).skip(skip).sort({ createdAt: -1 }).lean(),
        ]);
        const fields = ['_id', 'submitDate', 'merchandise.voucherCode', 'merchandise.rewardName', 'merchandise.offerId', 'merchandise.brandCode', 'ownerMobile', 'receiverAddress.address', 'receiverAddress.province', 'receiverAddress.district', 'receiverAddress.fullname', 'receiverAddress.mobile', 'status', 'deliveryDate', 'note'];
        const newFields = fields.map(field => field.split('.').pop());
        const newData = data.map(item => {
            const newItem = {};
            for (let i = 0; i < fields.length; i++) {
                newItem[newFields[i]] = (0, lodash_1.get)(item, fields[i], '');
            }
            return newItem;
        });
        return {
            data: newData,
            meta: {
                currentPage: +_page,
                pageSize: +_size,
                totalPages: Math.ceil(total / _size),
                totalRows: total,
            },
        };
    }
    async findByUserId(userId) {
        const orders = await this.orderModel.find({ userId }).sort({ createdAt: -1 }).lean();
        const result = [];
        let temp = [];
        for (let i = 0; i < orders.length; i++) {
            if (i === 0) {
                temp.push(orders[i]);
            }
            else {
                if (orders[i].submitDate.getTime() === orders[i - 1].submitDate.getTime()) {
                    temp.push(orders[i]);
                }
                else {
                    result.push(temp);
                    temp = [];
                    temp.push(orders[i]);
                }
            }
        }
        if (temp.length)
            result.push(temp);
        return result;
    }
    async create(order) {
        const result = await this.merchandiseService.redeemCoupons(order.ownerMobile, order.listMerchandise);
        order.submitDate = new Date();
        if (result.status.success) {
            for (const merchandise of order.listMerchandise) {
                const _order = Object.assign(Object.assign({}, order), { merchandise });
                delete _order.listMerchandise;
                await this.orderModel.create(_order);
            }
            this.logService.create({
                message: `Create ${order.listMerchandise.length} orders for user ${order.ownerMobile}`,
                type: constants_1.LogType.INFO,
            });
            await this.addressService.updateAddress(order.userId, order.receiverAddress);
        }
        return result;
    }
    async exportCsv(query) {
        const deliveryDate = new Date();
        const { delivered } = query, remain = __rest(query, ["delivered"]);
        const contentsToExport = await this.findAll(Object.assign(Object.assign({}, remain), { size: 1000000 }));
        try {
            if (delivered === 'true') {
                const ids = contentsToExport.data.map(item => item._id);
                await this.orderModel.updateMany({ _id: { $in: ids } }, { status: constants_1.OrderStatus.DELIVERED, deliveryDate });
                contentsToExport.data.forEach(item => {
                    item.status = constants_1.OrderStatus.DELIVERED;
                    item.deliveryDate = new Date();
                });
            }
            const json2csvParser = new json2csv_1.Parser();
            const csvContent = json2csvParser.parse(contentsToExport.data);
            if (delivered === 'true') {
                const uploadedFile = await this.mediaService.uploadCsvContentToS3(csvContent);
                if (uploadedFile)
                    await this.exportHistoryService.create({ deliveryDate, totalRows: contentsToExport.data.length, url: uploadedFile.url });
            }
            this.logService.create({
                message: `Return csv file with ${contentsToExport.data.length} rows (Upload to S3: ${delivered === 'true'})`,
                type: constants_1.LogType.INFO,
            });
            return csvContent;
        }
        catch (err) {
            console.error(err);
        }
    }
    async updateStatus(id, status) {
        const _order = await this.orderModel.findById(id);
        if (!_order) {
            this.logService.create({
                message: `Update status of order ${id} to ${status} failed`,
                type: constants_1.LogType.ERROR,
            });
            throw new common_1.HttpException('Order not found', common_1.HttpStatus.NOT_FOUND);
        }
        _order.status = status;
        await _order.save();
        this.logService.create({
            message: `Update status of order ${id} to ${status}`,
            type: constants_1.LogType.INFO,
        });
        return _order;
    }
};
OrderService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(order_schema_1.Order.name)),
    __metadata("design:paramtypes", [mongoose_2.Model, address_service_1.AddressService, merchandise_service_1.MerchandiseService, media_service_1.MediaService, export_history_service_1.ExportHistoryService, log_service_1.LogService])
], OrderService);
exports.OrderService = OrderService;
//# sourceMappingURL=order.service.js.map