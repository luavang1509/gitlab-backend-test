import { AddressDetailDto } from "@/address/dtos/address-detail.dto";
import { OrderStatus } from "src/constants";
import { MerchandiseDto } from "src/merchandise/dtos/merchandise.dto";
export declare class CreateMultipleOrderDto {
    userId: string;
    ownerMobile: string;
    listMerchandise: MerchandiseDto[];
    receiverAddress: AddressDetailDto;
    status: OrderStatus;
    deliveryDate: Date;
    note: string;
    submitDate: Date;
}
