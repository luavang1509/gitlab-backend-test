import { CreateMultipleOrderDto } from "./dtos/create-multiple-order.dto";
import { OrderService } from './order.service';
export declare class OrderController {
    private readonly orderService;
    constructor(orderService: OrderService);
    getOrder(query: any): Promise<any>;
    getOrderOfUserId(req: any): Promise<any[]>;
    createMultipleOrders(body: CreateMultipleOrderDto, req: any): Promise<any>;
    getOrderCsv(query: any): Promise<any>;
}
