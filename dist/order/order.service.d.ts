/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { Model } from 'mongoose';
import { Order, OrderDocument } from './schemas/order.schema';
import { AddressService } from "src/address/address.service";
import { MerchandiseService } from "src/merchandise/merchandise.service";
import { CreateMultipleOrderDto } from "./dtos/create-multiple-order.dto";
import { OrderStatus } from "src/constants";
import { MediaService } from "@/media/media.service";
import { ExportHistoryService } from "@/export-history/export-history.service";
import { LogService } from "@/log/log.service";
export declare class OrderService {
    private readonly orderModel;
    private readonly addressService;
    private readonly merchandiseService;
    private readonly mediaService;
    private readonly exportHistoryService;
    private readonly logService;
    constructor(orderModel: Model<OrderDocument>, addressService: AddressService, merchandiseService: MerchandiseService, mediaService: MediaService, exportHistoryService: ExportHistoryService, logService: LogService);
    findAll(query: any): Promise<any>;
    findByUserId(userId: string): Promise<any[]>;
    create(order: CreateMultipleOrderDto): Promise<any>;
    exportCsv(query: any): Promise<any>;
    updateStatus(id: string, status: OrderStatus): Promise<Order & import("mongoose").Document<any, any, any> & {
        _id: import("mongoose").Types.ObjectId;
    }>;
}
