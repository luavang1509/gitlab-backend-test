"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.USER_ACCESS_TOKEN_PREFIX = exports.ADMIN_ACCESS_TOKEN_PREFIX = exports.LogType = exports.OrderStatus = exports.Status = void 0;
var Status;
(function (Status) {
    Status["ISSUED"] = "Issued";
    Status["REDEEMED"] = "Redeemed";
    Status["EXPIRED"] = "Expired";
})(Status = exports.Status || (exports.Status = {}));
var OrderStatus;
(function (OrderStatus) {
    OrderStatus["NEW"] = "New";
    OrderStatus["DELIVERED"] = "Delivered";
    OrderStatus["CANCELLED"] = "Cancelled";
})(OrderStatus = exports.OrderStatus || (exports.OrderStatus = {}));
var LogType;
(function (LogType) {
    LogType["INFO"] = "Info";
    LogType["WARNING"] = "Warning";
    LogType["ERROR"] = "Error";
})(LogType = exports.LogType || (exports.LogType = {}));
exports.ADMIN_ACCESS_TOKEN_PREFIX = 'cmsadmin.access.token';
exports.USER_ACCESS_TOKEN_PREFIX = 'cmsuser.access.token';
//# sourceMappingURL=constants.js.map