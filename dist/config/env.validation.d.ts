import * as Joi from 'joi';
export declare const schema: {
    NODE_ENV: Joi.StringSchema<string>;
    APP_NAME: Joi.StringSchema<string>;
    PORT: Joi.NumberSchema<number>;
    API_LOGGER: Joi.StringSchema<string>;
    MONGODB_URI: Joi.StringSchema<string>;
    REDIS_HOST: Joi.StringSchema<string>;
    REDIS_PORT: Joi.NumberSchema<number>;
    REDIS_PREFIX: Joi.StringSchema<string>;
    ID_SERVICE_HOST: Joi.StringSchema<string>;
    ID_SERVICE_SECRET: Joi.StringSchema<string>;
    TCL_PROMOTION_SERVICE_HOST: Joi.StringSchema<string>;
    STORE_CODE: Joi.StringSchema<string>;
};
export declare type ConfigSchema = {
    [k in keyof typeof schema]: string;
};
export declare const configuration: () => {};
export declare const validationSchema: Joi.ObjectSchema<any>;
