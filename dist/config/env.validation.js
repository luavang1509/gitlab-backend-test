"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validationSchema = exports.configuration = exports.schema = void 0;
const Joi = require("joi");
exports.schema = {
    NODE_ENV: Joi.string().default('development'),
    APP_NAME: Joi.string().default('common_nest_project'),
    PORT: Joi.number().default(3000),
    API_LOGGER: Joi.string().default('true'),
    MONGODB_URI: Joi.string().required(),
    REDIS_HOST: Joi.string().required(),
    REDIS_PORT: Joi.number().required(),
    REDIS_PREFIX: Joi.string().required(),
    ID_SERVICE_HOST: Joi.string().required(),
    ID_SERVICE_SECRET: Joi.string().required(),
    TCL_PROMOTION_SERVICE_HOST: Joi.string().required(),
    STORE_CODE: Joi.string().required(),
};
const configuration = () => {
    return Object.keys(exports.schema).reduce((result, value) => {
        result[value] = process.env[value];
        return result;
    }, {});
};
exports.configuration = configuration;
exports.validationSchema = Joi.object(exports.schema);
//# sourceMappingURL=env.validation.js.map