"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogSchema = exports.LogModel = void 0;
const constants_1 = require("../../constants");
const mongoose_1 = require("@nestjs/mongoose");
let LogModel = class LogModel {
};
__decorate([
    (0, mongoose_1.Prop)({ type: String }),
    __metadata("design:type", String)
], LogModel.prototype, "action", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: String, required: true }),
    __metadata("design:type", String)
], LogModel.prototype, "message", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: String, enum: constants_1.LogType }),
    __metadata("design:type", String)
], LogModel.prototype, "type", void 0);
LogModel = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, collection: 'log-histories' })
], LogModel);
exports.LogModel = LogModel;
exports.LogSchema = mongoose_1.SchemaFactory.createForClass(LogModel);
//# sourceMappingURL=log.schema.js.map