"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logHelper = exports.LogHelper = void 0;
class LogHelper {
    log(message, ...optionalParams) {
        if (optionalParams && optionalParams.length) {
            console.log(message, optionalParams);
        }
        else {
            console.log(message);
        }
    }
    info(message, ...optionalParams) {
        if (optionalParams && optionalParams.length) {
            console.log(message, optionalParams);
        }
        else {
            console.log(message);
        }
    }
    error(message, ...optionalParams) {
        if (optionalParams && optionalParams.length) {
            console.error(message, optionalParams);
        }
        else {
            console.error(message);
        }
    }
}
exports.LogHelper = LogHelper;
exports.logHelper = new LogHelper();
//# sourceMappingURL=log.helper.js.map