import type { ConfigSchema } from '@/config/env.validation';
import { OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
export declare class RedisService implements OnApplicationShutdown, OnApplicationBootstrap {
    private configService;
    private logger;
    private client;
    constructor(configService: ConfigService<ConfigSchema>);
    onApplicationBootstrap(): Promise<void>;
    onApplicationShutdown(): Promise<void>;
    connect(): Promise<void>;
    disconnect(): Promise<void>;
    get(key: string): any;
    set(key: string, value: unknown): any;
    setEx(key: string, value: unknown, expire: number): any;
    setExNx(key: string, value: unknown, expire: number): any;
    del(key: string): any;
}
