"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var RedisService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const redis = require("redis");
let RedisService = RedisService_1 = class RedisService {
    constructor(configService) {
        this.configService = configService;
        this.logger = new common_1.Logger(RedisService_1.name);
    }
    async onApplicationBootstrap() {
        await this.connect();
    }
    async onApplicationShutdown() {
        await this.disconnect();
    }
    async connect() {
        let client = redis.createClient({
            socket: {
                host: this.configService.get('REDIS_HOST'),
                port: this.configService.get('REDIS_PORT'),
            },
        });
        (async () => {
            await client.connect();
        })();
        client.on('connect', () => {
            this.logger.log('Redis is connecting');
        });
        client.on('ready', () => {
            this.logger.log('Redis is ready');
        });
        client.on('error', (error) => {
            this.logger.error(error.message);
        });
        this.client = client;
    }
    async disconnect() {
        if (this.client && this.client.connected) {
            await this.client.quit();
        }
    }
    get(key) {
        return this.client.get(key);
    }
    set(key, value) {
        return this.client.set(key, value);
    }
    setEx(key, value, expire) {
        return this.client.set(key, value, {
            EX: expire,
        });
    }
    setExNx(key, value, expire) {
        return this.client.set(key, value, {
            EX: expire,
            NX: 10,
        });
    }
    del(key) {
        return this.client.del(key);
    }
};
RedisService = RedisService_1 = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], RedisService);
exports.RedisService = RedisService;
//# sourceMappingURL=redis.service.js.map