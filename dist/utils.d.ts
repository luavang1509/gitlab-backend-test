export declare const sha256: (input: string) => string;
export declare const md5: (input: string) => string;
export declare const generateToken: () => string;
export declare const generateUUID: () => string;
export declare const generateUUIDWithMoment: () => string;
export declare const getBearerToken: (header: string) => string;
export declare const hashPassword: (input: string) => string;
export declare const validatePassword: (input: string, hash: string) => boolean;
export declare const getFileExt: (filename: any) => {
    ext: string;
    normalize: string;
};
export declare function randomInt(min: number, max: number): number;
export declare const sanitizePageSize: (page: number, size: number) => {
    limit: number;
    skip: number;
};
export declare const arrayShuffle: (array: any[]) => any[];
export declare const findRangeOfNumber: (rangeArray: string[], number: number) => string;
export declare const generateFileName: (ext: string) => string;
export declare const parsePageNum: (v: any) => number;
export declare const getMimeFromFileName: (filename: any) => "image/jpeg" | "image/png" | "text/csv" | "application/octet-stream";
export declare const getExtFromFileName: (filename: any) => string;
