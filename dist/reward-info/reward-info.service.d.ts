import { Model } from 'mongoose';
import { RewardInfoDocument } from './schemas/reward-info.schema';
export declare class RewardInfoService {
    private readonly rewardInfoModel;
    constructor(rewardInfoModel: Model<RewardInfoDocument>);
    findAll(query: any): Promise<any>;
}
