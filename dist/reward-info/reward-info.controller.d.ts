import { RewardInfoService } from './reward-info.service';
export declare class RewardInfoController {
    private readonly rewardInfoService;
    constructor(rewardInfoService: RewardInfoService);
    getRewardInfo(query: any): Promise<any>;
}
