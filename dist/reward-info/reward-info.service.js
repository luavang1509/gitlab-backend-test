"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RewardInfoService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const lodash_1 = require("lodash");
const reward_info_schema_1 = require("./schemas/reward-info.schema");
const utils_1 = require("../utils");
let RewardInfoService = class RewardInfoService {
    constructor(rewardInfoModel) {
        this.rewardInfoModel = rewardInfoModel;
    }
    async findAll(query) {
        const _size = Number((0, lodash_1.get)(query, 'size', 10));
        const _page = Number((0, lodash_1.get)(query, 'page', 1));
        const { limit, skip } = (0, utils_1.sanitizePageSize)(_page, _size);
        const [total, data] = await Promise.all([
            this.rewardInfoModel.count(),
            this.rewardInfoModel.find().limit(limit).skip(skip).sort({ createdAt: -1 }).lean(),
        ]);
        return {
            data,
            meta: {
                currentPage: +_page,
                pageSize: +_size,
                totalPages: Math.ceil(total / _size),
                totalRows: total,
            },
        };
    }
};
RewardInfoService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)(reward_info_schema_1.RewardInfoModel.name)),
    __metadata("design:paramtypes", [mongoose_2.Model])
], RewardInfoService);
exports.RewardInfoService = RewardInfoService;
//# sourceMappingURL=reward-info.service.js.map