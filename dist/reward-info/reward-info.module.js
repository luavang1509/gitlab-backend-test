"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var RewardInfoModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RewardInfoModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const reward_info_controller_1 = require("./reward-info.controller");
const reward_info_service_1 = require("./reward-info.service");
const reward_info_schema_1 = require("./schemas/reward-info.schema");
const merchandise_module_1 = require("../merchandise/merchandise.module");
const address_module_1 = require("../address/address.module");
const common_module_1 = require("../common/common.module");
const media_module_1 = require("../media/media.module");
const export_history_module_1 = require("../export-history/export-history.module");
const log_module_1 = require("../log/log.module");
let RewardInfoModule = RewardInfoModule_1 = class RewardInfoModule {
};
RewardInfoModule = RewardInfoModule_1 = __decorate([
    (0, common_1.Module)({
        imports: [
            merchandise_module_1.MerchandiseModule,
            address_module_1.AddressModule,
            common_module_1.CommonModule,
            media_module_1.MediaModule,
            RewardInfoModule_1,
            export_history_module_1.ExportHistoryModule,
            log_module_1.LogModule,
            mongoose_1.MongooseModule.forFeature([
                { name: reward_info_schema_1.RewardInfoModel.name, schema: reward_info_schema_1.RewardInfoSchema },
            ]),
        ],
        controllers: [reward_info_controller_1.RewardInfoController],
        providers: [reward_info_service_1.RewardInfoService],
    })
], RewardInfoModule);
exports.RewardInfoModule = RewardInfoModule;
//# sourceMappingURL=reward-info.module.js.map