export declare enum Status {
    ISSUED = "Issued",
    REDEEMED = "Redeemed",
    EXPIRED = "Expired"
}
export declare enum OrderStatus {
    NEW = "New",
    DELIVERED = "Delivered",
    CANCELLED = "Cancelled"
}
export declare enum LogType {
    INFO = "Info",
    WARNING = "Warning",
    ERROR = "Error"
}
export declare const ADMIN_ACCESS_TOKEN_PREFIX = "cmsadmin.access.token";
export declare const USER_ACCESS_TOKEN_PREFIX = "cmsuser.access.token";
