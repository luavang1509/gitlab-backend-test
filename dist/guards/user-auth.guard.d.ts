import { CanActivate, ExecutionContext } from '@nestjs/common';
import { RedisService } from "../common/services/redis.service";
export declare class UserAuthGuard implements CanActivate {
    private redisService;
    constructor(redisService: RedisService);
    canActivate(context: ExecutionContext): Promise<boolean>;
    validateToken(token: string): Promise<any>;
}
