"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminAuthGuard = void 0;
const constants_1 = require("../constants");
const common_1 = require("@nestjs/common");
const redis_service_1 = require("../common/services/redis.service");
const utils_1 = require("../utils");
let AdminAuthGuard = class AdminAuthGuard {
    constructor(redisService) {
        this.redisService = redisService;
    }
    async canActivate(context) {
        const request = context.switchToHttp().getRequest();
        const header = request.header('Authorization');
        const token = (0, utils_1.getBearerToken)(header);
        const user = await this.validateToken(token);
        if (user) {
            request.user = user;
            return true;
        }
        throw new common_1.ForbiddenException();
    }
    async validateToken(token) {
        if (token) {
            const key = `${constants_1.ADMIN_ACCESS_TOKEN_PREFIX}.${token}`;
            const data = await this.redisService.get(key);
            if (data) {
                const user = JSON.parse(data);
                return user;
            }
        }
        return null;
    }
};
AdminAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [redis_service_1.RedisService])
], AdminAuthGuard);
exports.AdminAuthGuard = AdminAuthGuard;
//# sourceMappingURL=admin-auth.guard.js.map