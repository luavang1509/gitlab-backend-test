export declare class AddressDetailDto {
    address: string;
    province: string;
    district: string;
    mobile: string;
    fullname: string;
}
