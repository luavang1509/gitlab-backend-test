"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaModule = void 0;
const common_1 = require("@nestjs/common");
const common_module_1 = require("../common/common.module");
const media_service_1 = require("./media.service");
const media_controller_1 = require("./media.controller");
const mongoose_1 = require("@nestjs/mongoose");
const upload_file_schema_1 = require("./schemas/upload-file.schema");
const log_module_1 = require("../log/log.module");
let MediaModule = class MediaModule {
};
MediaModule = __decorate([
    (0, common_1.Module)({
        imports: [
            common_module_1.CommonModule,
            mongoose_1.MongooseModule.forFeature([
                { name: upload_file_schema_1.UploadFileModel.name, schema: upload_file_schema_1.UploadFileSchema },
            ]),
            log_module_1.LogModule
        ],
        providers: [media_service_1.MediaService],
        exports: [media_service_1.MediaService],
        controllers: [media_controller_1.MediaController],
    })
], MediaModule);
exports.MediaModule = MediaModule;
//# sourceMappingURL=media.module.js.map