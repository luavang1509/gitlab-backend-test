/// <reference types="multer" />
export declare const UploadImage: {
    limits: {
        fieldNameSize: number;
        files: number;
        fileSize: number;
    };
    storage: import("multer").StorageEngine;
    fileFilter: (req: any, file: any, cb: any) => Promise<any>;
};
export declare const UploadCSV: {
    limits: {
        fieldNameSize: number;
        files: number;
        fileSize: number;
    };
    fileFilter: (req: any, file: any, cb: any) => Promise<any>;
    storage: import("multer").StorageEngine;
};
