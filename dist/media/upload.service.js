"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadCSV = exports.UploadImage = void 0;
const multer_1 = require("multer");
const utils_1 = require("../utils");
const path = require("path");
const fs = require("fs-extra");
const moment = require("moment-timezone");
const enums_1 = require("../enums");
const MAX_SIZE = 1024 * 1024 * 1;
const USER_CACHE_IMAGE_DIRECTORY = path.resolve(__dirname, '../../', path.join('data', '.cache', '.images'));
const USER_CACHE_FILE_DIRECTORY = path.resolve(__dirname, '../../', path.join('data', '.cache', '.files'));
let __currentImageCacheDirPath = null;
let __currentFileCacheDirPath = null;
async function getImageCacheDirectory() {
    const directory = path.join(USER_CACHE_IMAGE_DIRECTORY, moment().format('YYYY-MM-DD'));
    if (__currentImageCacheDirPath === null ||
        __currentImageCacheDirPath !== directory) {
        await fs.ensureDir(directory);
        __currentImageCacheDirPath = directory;
        return directory;
    }
    return __currentImageCacheDirPath;
}
async function getFileCacheDirectory() {
    const directory = path.join(USER_CACHE_FILE_DIRECTORY, moment().format('YYYY-MM-DD'));
    if (__currentFileCacheDirPath === null ||
        __currentFileCacheDirPath !== directory) {
        await fs.ensureDir(directory);
        __currentFileCacheDirPath = directory;
        return directory;
    }
    return __currentFileCacheDirPath;
}
const FileDiskStorage = (0, multer_1.diskStorage)({
    destination: async function (_request, file, cb) {
        try {
            const filePath = await getFileCacheDirectory();
            file.filePath = filePath;
            cb(null, filePath);
        }
        catch (e) {
            cb(e, null);
        }
    },
    filename: (request, file, cb) => getFileName(request, file, cb),
});
const ImageDiskStorage = (0, multer_1.diskStorage)({
    destination: async function (_request, file, cb) {
        try {
            const filePath = await getImageCacheDirectory();
            file.filePath = filePath;
            cb(null, filePath);
        }
        catch (e) {
            cb(e, null);
        }
    },
    filename: (request, file, cb) => getFileName(request, file, cb),
});
function getFileName(_request, file, cb) {
    const extend = (0, utils_1.getFileExt)(file.originalname);
    const hash = (0, utils_1.md5)((0, utils_1.generateUUIDWithMoment)());
    file.hash = hash;
    file.name = hash + extend.normalize;
    file.ext = extend.normalize;
    file.filePath = path.join(file.filePath, file.name);
    cb(null, file.name);
}
exports.UploadImage = {
    limits: {
        fieldNameSize: 1024,
        files: 1,
        fileSize: MAX_SIZE,
    },
    storage: ImageDiskStorage,
    fileFilter: async (req, file, cb) => {
        const mimetype = file.mimetype.toLowerCase();
        if (!mimetype.includes('jpeg') &&
            !mimetype.includes('jpg') &&
            !mimetype.includes('png')) {
            req.fileValidationError = enums_1.MessageError['IMAGE_INVALID_FORMAT'];
            return cb(null, false, enums_1.MessageError['IMAGE_INVALID_FORMAT']);
        }
        else {
            cb(null, true);
        }
    },
};
exports.UploadCSV = {
    limits: {
        fieldNameSize: 1024,
        files: 1,
        fileSize: MAX_SIZE,
    },
    fileFilter: async (req, file, cb) => {
        const mimetype = file.mimetype.toLowerCase();
        if (mimetype.includes('csv') || mimetype.includes('vnd.ms-excel')) {
            cb(null, true);
        }
        else {
            req.fileValidationError = enums_1.MessageError['CSV_INVALID_FORMAT'];
            return cb(null, false, enums_1.MessageError['CSV_INVALID_FORMAT']);
        }
    },
    storage: FileDiskStorage,
};
//# sourceMappingURL=upload.service.js.map