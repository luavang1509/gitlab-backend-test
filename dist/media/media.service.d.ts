/// <reference types="multer" />
import { ConfigService } from '@nestjs/config';
import * as AWS from 'aws-sdk';
import { UploadFileDocument } from './schemas/upload-file.schema';
import { Model } from 'mongoose';
import { LogService } from "@/log/log.service";
export declare class MediaService {
    private readonly configService;
    private uploadFileModel;
    private readonly logService;
    private s3;
    constructor(configService: ConfigService, uploadFileModel: Model<UploadFileDocument>, logService: LogService);
    parseFileData(file: Express.Multer.File, fieldsName: string[]): Promise<unknown>;
    getS3(): AWS.S3;
    sendFileToS3(fileStream: any, file: Express.Multer.File): Promise<{
        name: string;
        url: string;
        ext: string;
        hash: string;
        mime: string;
        size: number;
    }>;
    uploadCsvContentToS3(csvContent: string): Promise<{
        name: string;
        url: string;
        ext: string;
        hash: string;
        mime: string;
        size: number;
    }>;
    upload(file: Express.Multer.File): Promise<{
        name: string;
        url: string;
        ext: string;
        hash: string;
        mime: string;
        size: number;
    }>;
}
