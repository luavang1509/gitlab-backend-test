/// <reference types="multer" />
import { MediaService } from './media.service';
export declare class MediaController {
    private readonly mediaService;
    private readonly logger;
    constructor(mediaService: MediaService);
    uploadCsv(req: any, file: Express.Multer.File): Promise<{
        data: {
            name: string;
            url: string;
            ext: string;
            hash: string;
            mime: string;
            size: number;
        };
    }>;
    uploadImage(req: any, file: Express.Multer.File): Promise<{
        data: {
            name: string;
            url: string;
            ext: string;
            hash: string;
            mime: string;
            size: number;
        };
    }>;
}
