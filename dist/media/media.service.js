"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaService = void 0;
const common_1 = require("@nestjs/common");
const csv = require("csv-parser");
const stream_1 = require("stream");
const fs = require("fs-extra");
const config_1 = require("@nestjs/config");
const AWS = require("aws-sdk");
const utils_1 = require("../utils");
const mongoose_1 = require("@nestjs/mongoose");
const upload_file_schema_1 = require("./schemas/upload-file.schema");
const mongoose_2 = require("mongoose");
const log_service_1 = require("../log/log.service");
const constants_1 = require("../constants");
let MediaService = class MediaService {
    constructor(configService, uploadFileModel, logService) {
        this.configService = configService;
        this.uploadFileModel = uploadFileModel;
        this.logService = logService;
    }
    parseFileData(file, fieldsName) {
        return new Promise((resolve, reject) => {
            const results = [];
            const options = {
                skipLines: 1,
                separator: ',',
                headers: fieldsName,
            };
            stream_1.Readable.from(fs.createReadStream(file.path))
                .pipe(csv(options))
                .on('data', (data) => {
                const item = {};
                fieldsName.forEach((v) => {
                    if (data[v]) {
                        item[v] = data[v];
                    }
                });
                results.push(item);
            })
                .on('error', (err) => reject(err))
                .on('end', () => resolve(results));
        });
    }
    getS3() {
        if (!this.s3) {
            this.s3 = new AWS.S3({
                accessKeyId: this.configService.get('AWS_ACCESS_KEY_ID'),
                secretAccessKey: this.configService.get('AWS_SECRET_ACCESS_KEY'),
                region: this.configService.get('AWS_REGION'),
            });
        }
        return this.s3;
    }
    async sendFileToS3(fileStream, file) {
        const { originalname } = file;
        const ext = (0, utils_1.getExtFromFileName)(originalname);
        const name = (0, utils_1.generateFileName)(ext);
        const mime = (0, utils_1.getMimeFromFileName)(originalname);
        const bucket = this.configService.get('AWS_BUCKET') + (mime.includes('image') ? '/images' : '/csv');
        const params = {
            Bucket: bucket,
            Key: name,
            Body: fileStream,
            ContentType: mime,
            Metadata: { mimetype: mime },
            CacheControl: 'max-age=3600',
        };
        const uploaded = await this.getS3().upload(params).promise();
        if (uploaded) {
            this.logService.create({
                message: `Uploaded file ${originalname} to S3`,
                type: constants_1.LogType.INFO,
            });
            const [hash] = name.split('.');
            const { size } = file;
            const url = await this.getS3().getSignedUrlPromise('getObject', {
                Bucket: bucket,
                Key: name,
                Expires: 60 * 60 * 24 * 7,
            });
            const ext = (0, utils_1.getExtFromFileName)(name);
            await this.uploadFileModel.create({
                name,
                mime,
                hash,
                url,
                ext,
                size,
                isRemoved: 0,
            });
            return { name, url, ext, hash, mime, size };
        }
        this.logService.create({
            message: 'Error while uploading file to S3',
            type: constants_1.LogType.ERROR,
        });
        return null;
    }
    async uploadCsvContentToS3(csvContent) {
        const bucket = this.configService.get('AWS_BUCKET') + '/csv';
        const name = (0, utils_1.generateFileName)('.csv');
        const mime = 'text/csv';
        const params = {
            Bucket: bucket,
            Key: name,
            Body: csvContent,
            ContentType: mime,
            Metadata: { mimetype: 'text/csv' },
            CacheControl: 'max-age=3600',
        };
        const uploaded = await this.getS3().upload(params).promise();
        if (uploaded) {
            this.logService.create({
                message: `Uploaded csv content to S3`,
                type: constants_1.LogType.INFO,
            });
            const [hash] = name.split('.');
            const url = await this.getS3().getSignedUrlPromise('getObject', {
                Bucket: bucket,
                Key: name,
                Expires: 60 * 60 * 24 * 7,
            });
            const ext = (0, utils_1.getExtFromFileName)(name);
            await this.uploadFileModel.create({
                name,
                mime,
                hash,
                url,
                ext,
                size: 0,
                isRemoved: 0,
            });
            return { name, url, ext, hash, mime, size: 0 };
        }
        this.logService.create({
            message: 'Error while uploading csv to S3',
            type: constants_1.LogType.ERROR,
        });
        return null;
    }
    async upload(file) {
        const { path } = file;
        const fileStream = fs.createReadStream(path);
        return this.sendFileToS3(fileStream, file);
    }
};
MediaService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, mongoose_1.InjectModel)(upload_file_schema_1.UploadFileModel.name)),
    __metadata("design:paramtypes", [config_1.ConfigService,
        mongoose_2.Model,
        log_service_1.LogService])
], MediaService);
exports.MediaService = MediaService;
//# sourceMappingURL=media.service.js.map