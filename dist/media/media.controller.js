"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var MediaController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaController = void 0;
const common_1 = require("@nestjs/common");
const media_service_1 = require("./media.service");
const upload_service_1 = require("./upload.service");
const platform_express_1 = require("@nestjs/platform-express");
let MediaController = MediaController_1 = class MediaController {
    constructor(mediaService) {
        this.mediaService = mediaService;
        this.logger = new common_1.Logger(MediaController_1.name);
    }
    async uploadCsv(req, file) {
        if (req.fileValidationError) {
            throw new common_1.BadRequestException(req.fileValidationError);
        }
        const data = await this.mediaService.upload(file);
        return { data };
    }
    async uploadImage(req, file) {
        if (req.fileValidationError) {
            throw new common_1.BadRequestException(req.fileValidationError);
        }
        const data = await this.mediaService.upload(file);
        return { data };
    }
};
__decorate([
    (0, common_1.Post)('/csv'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file', upload_service_1.UploadCSV)),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MediaController.prototype, "uploadCsv", null);
__decorate([
    (0, common_1.Post)('/image'),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('file', upload_service_1.UploadImage)),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MediaController.prototype, "uploadImage", null);
MediaController = MediaController_1 = __decorate([
    (0, common_1.Controller)('media'),
    __metadata("design:paramtypes", [media_service_1.MediaService])
], MediaController);
exports.MediaController = MediaController;
//# sourceMappingURL=media.controller.js.map